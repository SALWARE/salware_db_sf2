----------------------------------------------------------------------
-- Created by SmartDesign Sat Feb 11 14:21:05 2017
-- Version: v11.7 11.7.0.119
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library smartfusion2;
use smartfusion2.all;
----------------------------------------------------------------------
-- lvds_in entity declaration
----------------------------------------------------------------------
entity lvds_in is
    -- Port list
    port(
        -- Inputs
        PADN_IN : in  std_logic_vector(0 to 0);
        PADP_IN : in  std_logic_vector(0 to 0);
        -- Outputs
        Y       : out std_logic_vector(0 to 0)
        );
end lvds_in;
----------------------------------------------------------------------
-- lvds_in architecture body
----------------------------------------------------------------------
architecture RTL of lvds_in is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- lvds_in_lvds_in_0_IO   -   Actel:SgCore:IO:1.0.101
component lvds_in_lvds_in_0_IO
    -- Port list
    port(
        -- Inputs
        PADN_IN : in  std_logic_vector(0 to 0);
        PADP_IN : in  std_logic_vector(0 to 0);
        -- Outputs
        Y       : out std_logic_vector(0 to 0)
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal Y_net_0 : std_logic_vector(0 to 0);
signal Y_net_1 : std_logic_vector(0 to 0);
----------------------------------------------------------------------
-- TiedOff Signals
----------------------------------------------------------------------
signal GND_net : std_logic;

begin
----------------------------------------------------------------------
-- Constant assignments
----------------------------------------------------------------------
 GND_net    <= '0';
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 Y_net_1(0) <= Y_net_0(0);
 Y(0)       <= Y_net_1(0);
----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- lvds_in_0   -   Actel:SgCore:IO:1.0.101
lvds_in_0 : lvds_in_lvds_in_0_IO
    port map( 
        -- Inputs
        PADP_IN => PADP_IN,
        PADN_IN => PADN_IN,
        -- Outputs
        Y       => Y_net_0 
        );

end RTL;
