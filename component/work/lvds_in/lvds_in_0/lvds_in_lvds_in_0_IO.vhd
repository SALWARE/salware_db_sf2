-- Version: v11.7 11.7.0.119

library ieee;
use ieee.std_logic_1164.all;
library smartfusion2;
use smartfusion2.all;

entity lvds_in_lvds_in_0_IO is

    port( PADP_IN : in    std_logic_vector(0 to 0);
          PADN_IN : in    std_logic_vector(0 to 0);
          Y       : out   std_logic_vector(0 to 0)
        );

end lvds_in_lvds_in_0_IO;

architecture DEF_ARCH of lvds_in_lvds_in_0_IO is 

  component INBUF_DIFF
    generic (IOSTD:string := "");

    port( PADP : in    std_logic := 'U';
          PADN : in    std_logic := 'U';
          Y    : out   std_logic
        );
  end component;


begin 


    U0_0 : INBUF_DIFF
      generic map(IOSTD => "LVDS")

      port map(PADP => PADP_IN(0), PADN => PADN_IN(0), Y => Y(0));
    

end DEF_ARCH; 
