----------------------------------------------------------------------
-- Created by SmartDesign Fri Oct 14 19:19:07 2016
-- Version: v11.7 11.7.0.119
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library smartfusion2;
use smartfusion2.all;
----------------------------------------------------------------------
-- clk_buf entity declaration
----------------------------------------------------------------------
entity clk_buf is
    -- Port list
    port(
        -- Inputs
        CLK0 : in  std_logic;
        -- Outputs
        GL0  : out std_logic
        );
end clk_buf;
----------------------------------------------------------------------
-- clk_buf architecture body
----------------------------------------------------------------------
architecture RTL of clk_buf is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- clk_buf_clk_buf_0_FCCC   -   Actel:SgCore:FCCC:2.0.200
component clk_buf_clk_buf_0_FCCC
    -- Port list
    port(
        -- Inputs
        CLK0 : in  std_logic;
        -- Outputs
        GL0  : out std_logic
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal GL0_net_0 : std_logic;
signal GL0_net_1 : std_logic;
----------------------------------------------------------------------
-- TiedOff Signals
----------------------------------------------------------------------
signal GND_net   : std_logic;
signal PADDR_const_net_0: std_logic_vector(7 downto 2);
signal PWDATA_const_net_0: std_logic_vector(7 downto 0);

begin
----------------------------------------------------------------------
-- Constant assignments
----------------------------------------------------------------------
 GND_net            <= '0';
 PADDR_const_net_0  <= B"000000";
 PWDATA_const_net_0 <= B"00000000";
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 GL0_net_1 <= GL0_net_0;
 GL0       <= GL0_net_1;
----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- clk_buf_0   -   Actel:SgCore:FCCC:2.0.200
clk_buf_0 : clk_buf_clk_buf_0_FCCC
    port map( 
        -- Inputs
        CLK0 => CLK0,
        -- Outputs
        GL0  => GL0_net_0 
        );

end RTL;
