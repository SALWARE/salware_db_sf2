set_component clk_buf_clk_buf_0_FCCC
# Microsemi Corp.
# Date: 2016-Oct-14 19:19:06
#

create_clock -period 48.1928 [ get_pins { CCC_INST/CLK0 } ]
create_generated_clock -divide_by 1 -source [ get_pins { CCC_INST/CLK0 } ] [ get_pins { CCC_INST/GL0 } ]
