-- Version: v11.7 11.7.0.119

library ieee;
use ieee.std_logic_1164.all;
library smartfusion2;
use smartfusion2.all;

entity out_buf_out_buf_0_IO is

    port( PAD_OUT : out   std_logic_vector(0 to 0);
          D       : in    std_logic_vector(0 to 0)
        );

end out_buf_out_buf_0_IO;

architecture DEF_ARCH of out_buf_out_buf_0_IO is 

  component OUTBUF
    generic (IOSTD:string := "");

    port( D   : in    std_logic := 'U';
          PAD : out   std_logic
        );
  end component;


begin 


    U0_0 : OUTBUF
      generic map(IOSTD => "LVCMOS25")

      port map(D => D(0), PAD => PAD_OUT(0));
    

end DEF_ARCH; 
