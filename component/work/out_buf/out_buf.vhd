----------------------------------------------------------------------
-- Created by SmartDesign Sat Feb 11 14:01:22 2017
-- Version: v11.7 11.7.0.119
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library smartfusion2;
use smartfusion2.all;
----------------------------------------------------------------------
-- out_buf entity declaration
----------------------------------------------------------------------
entity out_buf is
    -- Port list
    port(
        -- Inputs
        D       : in  std_logic_vector(0 to 0);
        -- Outputs
        PAD_OUT : out std_logic_vector(0 to 0)
        );
end out_buf;
----------------------------------------------------------------------
-- out_buf architecture body
----------------------------------------------------------------------
architecture RTL of out_buf is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- out_buf_out_buf_0_IO   -   Actel:SgCore:IO:1.0.101
component out_buf_out_buf_0_IO
    -- Port list
    port(
        -- Inputs
        D       : in  std_logic_vector(0 to 0);
        -- Outputs
        PAD_OUT : out std_logic_vector(0 to 0)
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal PAD_OUT_net_0 : std_logic_vector(0 to 0);
signal PAD_OUT_net_1 : std_logic_vector(0 to 0);
----------------------------------------------------------------------
-- TiedOff Signals
----------------------------------------------------------------------
signal GND_net       : std_logic;

begin
----------------------------------------------------------------------
-- Constant assignments
----------------------------------------------------------------------
 GND_net    <= '0';
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 PAD_OUT_net_1(0) <= PAD_OUT_net_0(0);
 PAD_OUT(0)       <= PAD_OUT_net_1(0);
----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- out_buf_0   -   Actel:SgCore:IO:1.0.101
out_buf_0 : out_buf_out_buf_0_IO
    port map( 
        -- Inputs
        D       => D,
        -- Outputs
        PAD_OUT => PAD_OUT_net_0 
        );

end RTL;
