----------------------------------------------------------------------
-- Created by SmartDesign Sat Feb 11 13:59:39 2017
-- Version: v11.7 11.7.0.119
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library smartfusion2;
use smartfusion2.all;
----------------------------------------------------------------------
-- lvds_out entity declaration
----------------------------------------------------------------------
entity lvds_out is
    -- Port list
    port(
        -- Inputs
        D        : in  std_logic_vector(0 to 0);
        -- Outputs
        PADN_OUT : out std_logic_vector(0 to 0);
        PADP_OUT : out std_logic_vector(0 to 0)
        );
end lvds_out;
----------------------------------------------------------------------
-- lvds_out architecture body
----------------------------------------------------------------------
architecture RTL of lvds_out is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- lvds_out_lvds_out_0_IO   -   Actel:SgCore:IO:1.0.101
component lvds_out_lvds_out_0_IO
    -- Port list
    port(
        -- Inputs
        D        : in  std_logic_vector(0 to 0);
        -- Outputs
        PADN_OUT : out std_logic_vector(0 to 0);
        PADP_OUT : out std_logic_vector(0 to 0)
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal PADN_OUT_net_0 : std_logic_vector(0 to 0);
signal PADP_OUT_net_0 : std_logic_vector(0 to 0);
signal PADP_OUT_net_1 : std_logic_vector(0 to 0);
signal PADN_OUT_net_1 : std_logic_vector(0 to 0);
----------------------------------------------------------------------
-- TiedOff Signals
----------------------------------------------------------------------
signal GND_net        : std_logic;

begin
----------------------------------------------------------------------
-- Constant assignments
----------------------------------------------------------------------
 GND_net    <= '0';
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 PADP_OUT_net_1(0) <= PADP_OUT_net_0(0);
 PADP_OUT(0)       <= PADP_OUT_net_1(0);
 PADN_OUT_net_1(0) <= PADN_OUT_net_0(0);
 PADN_OUT(0)       <= PADN_OUT_net_1(0);
----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- lvds_out_0   -   Actel:SgCore:IO:1.0.101
lvds_out_0 : lvds_out_lvds_out_0_IO
    port map( 
        -- Inputs
        D        => D,
        -- Outputs
        PADP_OUT => PADP_OUT_net_0,
        PADN_OUT => PADN_OUT_net_0 
        );

end RTL;
