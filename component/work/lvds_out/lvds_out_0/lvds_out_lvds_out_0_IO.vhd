-- Version: v11.7 11.7.0.119

library ieee;
use ieee.std_logic_1164.all;
library smartfusion2;
use smartfusion2.all;

entity lvds_out_lvds_out_0_IO is

    port( PADP_OUT : out   std_logic_vector(0 to 0);
          PADN_OUT : out   std_logic_vector(0 to 0);
          D        : in    std_logic_vector(0 to 0)
        );

end lvds_out_lvds_out_0_IO;

architecture DEF_ARCH of lvds_out_lvds_out_0_IO is 

  component OUTBUF_DIFF
    generic (IOSTD:string := "");

    port( D    : in    std_logic := 'U';
          PADP : out   std_logic;
          PADN : out   std_logic
        );
  end component;


begin 


    U0_0 : OUTBUF_DIFF
      generic map(IOSTD => "LVDS")

      port map(D => D(0), PADP => PADP_OUT(0), PADN => 
        PADN_OUT(0));
    

end DEF_ARCH; 
