set_component ex_osc_ex_osc_0_OSC
# Microsemi Corp.
# Date: 2017-Mar-07 13:42:12
#

create_clock -period 83.3333 [ get_pins { I_XTLOSC/CLKOUT } ]
