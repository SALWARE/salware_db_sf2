----------------------------------------------------------------------
-- Created by SmartDesign Tue Mar 07 13:42:12 2017
-- Version: v11.7 11.7.0.119
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library smartfusion2;
use smartfusion2.all;
----------------------------------------------------------------------
-- ex_osc entity declaration
----------------------------------------------------------------------
entity ex_osc is
    -- Port list
    port(
        -- Inputs
        XTL        : in  std_logic;
        -- Outputs
        XTLOSC_O2F : out std_logic
        );
end ex_osc;
----------------------------------------------------------------------
-- ex_osc architecture body
----------------------------------------------------------------------
architecture RTL of ex_osc is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- ex_osc_ex_osc_0_OSC   -   Actel:SgCore:OSC:2.0.101
component ex_osc_ex_osc_0_OSC
    -- Port list
    port(
        -- Inputs
        XTL                : in  std_logic;
        -- Outputs
        RCOSC_1MHZ_CCC     : out std_logic;
        RCOSC_1MHZ_O2F     : out std_logic;
        RCOSC_25_50MHZ_CCC : out std_logic;
        RCOSC_25_50MHZ_O2F : out std_logic;
        XTLOSC_CCC         : out std_logic;
        XTLOSC_O2F         : out std_logic
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal XTLOSC_O2F_net_0 : std_logic;
signal XTLOSC_O2F_net_1 : std_logic;

begin
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 XTLOSC_O2F_net_1 <= XTLOSC_O2F_net_0;
 XTLOSC_O2F       <= XTLOSC_O2F_net_1;
----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- ex_osc_0   -   Actel:SgCore:OSC:2.0.101
ex_osc_0 : ex_osc_ex_osc_0_OSC
    port map( 
        -- Inputs
        XTL                => XTL,
        -- Outputs
        RCOSC_25_50MHZ_CCC => OPEN,
        RCOSC_25_50MHZ_O2F => OPEN,
        RCOSC_1MHZ_CCC     => OPEN,
        RCOSC_1MHZ_O2F     => OPEN,
        XTLOSC_CCC         => OPEN,
        XTLOSC_O2F         => XTLOSC_O2F_net_0 
        );

end RTL;
