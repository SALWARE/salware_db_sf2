----------------------------------------------------------------------
-- Created by SmartDesign Thu Feb 23 00:19:39 2017
-- Version: v11.7 11.7.0.119
----------------------------------------------------------------------

----------------------------------------------------------------------
-- Libraries
----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

library smartfusion2;
use smartfusion2.all;
----------------------------------------------------------------------
-- pll entity declaration
----------------------------------------------------------------------
entity pll is
    -- Port list
    port(
        -- Inputs
        XTLOSC : in  std_logic;
        -- Outputs
        GL0    : out std_logic;
        LOCK   : out std_logic
        );
end pll;
----------------------------------------------------------------------
-- pll architecture body
----------------------------------------------------------------------
architecture RTL of pll is
----------------------------------------------------------------------
-- Component declarations
----------------------------------------------------------------------
-- pll_pll_0_FCCC   -   Actel:SgCore:FCCC:2.0.201
component pll_pll_0_FCCC
    -- Port list
    port(
        -- Inputs
        XTLOSC : in  std_logic;
        -- Outputs
        GL0    : out std_logic;
        LOCK   : out std_logic
        );
end component;
----------------------------------------------------------------------
-- Signal declarations
----------------------------------------------------------------------
signal GL0_net_0  : std_logic;
signal LOCK_net_0 : std_logic;
signal GL0_net_1  : std_logic;
signal LOCK_net_1 : std_logic;
----------------------------------------------------------------------
-- TiedOff Signals
----------------------------------------------------------------------
signal GND_net    : std_logic;
signal PADDR_const_net_0: std_logic_vector(7 downto 2);
signal PWDATA_const_net_0: std_logic_vector(7 downto 0);

begin
----------------------------------------------------------------------
-- Constant assignments
----------------------------------------------------------------------
 GND_net            <= '0';
 PADDR_const_net_0  <= B"000000";
 PWDATA_const_net_0 <= B"00000000";
----------------------------------------------------------------------
-- Top level output port assignments
----------------------------------------------------------------------
 GL0_net_1  <= GL0_net_0;
 GL0        <= GL0_net_1;
 LOCK_net_1 <= LOCK_net_0;
 LOCK       <= LOCK_net_1;
----------------------------------------------------------------------
-- Component instances
----------------------------------------------------------------------
-- pll_0   -   Actel:SgCore:FCCC:2.0.201
pll_0 : pll_pll_0_FCCC
    port map( 
        -- Inputs
        XTLOSC => XTLOSC,
        -- Outputs
        GL0    => GL0_net_0,
        LOCK   => LOCK_net_0 
        );

end RTL;
