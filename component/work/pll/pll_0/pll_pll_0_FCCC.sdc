set_component pll_pll_0_FCCC
# Microsemi Corp.
# Date: 2017-Feb-23 00:19:38
#

create_clock -period 83.3333 [ get_pins { CCC_INST/XTLOSC } ]
create_generated_clock -multiply_by 50 -divide_by 3 -source [ get_pins { CCC_INST/XTLOSC } ] -phase 0 [ get_pins { CCC_INST/GL0 } ]
