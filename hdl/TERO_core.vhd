-- **********************************************************************************************
-- ** Copyright (c) Laboratoire Hubert Curien,  All rights reserved.                           **
-- **                                                                                          **
-- **                                                                                          **
-- ** The data exchange is covered by the HECTOR project NDA.                                  **
-- ** The data provided by us are for use within the HECTOR project only and are sole IP       **
-- ** of Laboratoire Hubert Curien. Any other use or distribution                              **
-- ** thereof has to be granted by us and otherwise will be in violation of the  project non   **
-- ** disclosure agreement.                                                                    **
-- **                                                                                          **
-- **********************************************************************************************
---------------------------------------------------------------------
--
-- Design unit:   TERO core
--
-- File name:     TERO_core.vhd
--
-- Description:   generate a unique TERO cell made of AND2 + NAND2 smartfusion2 library primitives
--
-- Autor:         Ugo Mureddu, Nathalie Bochard, Hubert Curien Laboratory, France
--
-- Copyright:     Hubert Curien Laboratory
--
-- Revision:      Version 1.00, June 2016
--
---------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

library smartfusion2;
use smartfusion2.all;

ENTITY TERO_core IS
  GENERIC (
    length : INTEGER := 4
  );
  PORT( 
    ctrl           : IN  STD_LOGIC := '1';       -- Desables oscillator output
    tero_out       : OUT STD_LOGIC               -- Oscillator output
    );
END TERO_core;

-------------------------------------------------------------------------------  
ARCHITECTURE rtl OF TERO_core IS
-------------------------------------------------------------------------------  

COMPONENT buff_wrp IS
  PORT (
    i : IN  STD_LOGIC;
    o : OUT STD_LOGIC
  );
END COMPONENT buff_wrp;

-- NAND2
COMPONENT NAND2 IS
  -- Port list
  PORT(
    -- Inputs
    A : IN  STD_LOGIC;
    B : IN  STD_LOGIC;
    -- Outputs
    Y : OUT STD_LOGIC
  );
END COMPONENT;

COMPONENT DFN1C0 IS
  PORT (
    D   : IN  STD_LOGIC;
    CLK : IN  STD_LOGIC;
    CLR : IN  STD_LOGIC;
    Q   : OUT STD_LOGIC
  );
END COMPONENT;

COMPONENT INV IS
  PORT (
    A : IN  STD_LOGIC;
    Y : OUT STD_LOGIC
  );
END COMPONENT;

ATTRIBUTE keep : BOOLEAN;

SIGNAL del1, del2  : STD_LOGIC_VECTOR(length-1 DOWNTO 0):=('1', OTHERS => '0');
--SIGNAL dff_out     : STD_LOGIC := '0';
--SIGNAL tff_out     : STD_LOGIC := '0';

ATTRIBUTE keep OF del1 : SIGNAL IS TRUE;
ATTRIBUTE keep OF del2 : SIGNAL IS TRUE;

------------------------------------------------------------------------------- 
BEGIN

ASSERT (length >= 2)
  REPORT "RO must contain at least 2 elements. Current length="&integer'image(length)
  SEVERITY ERROR;
------------------------------------------------------------------------------- 

-- BUFFERS
gen_buff1 : FOR ii IN 0 TO length-2 GENERATE
  buff_1 : buff_wrp
    PORT MAP 
    (
      i => del1(ii),
      o => del1(ii+1)
    );
END GENERATE gen_buff1;

gen_buff2 : FOR ii IN 0 TO length-2 GENERATE
  buff_2 : buff_wrp
    PORT MAP 
    (
      i => del2(ii),
      o => del2(ii+1)
    );
END GENERATE gen_buff2;

nand_1 : NAND2
  PORT MAP(
    A => del1(length-1),
    B => ctrl,
    Y => del2(0)
  );
--
nand_2 : NAND2
  PORT MAP(
    A => del2(length-1),
    B => ctrl,
    Y => del1(0)
  );

tero_out <= del2(0);



---- DIVIDER by 2
  --
  --dff_i : DFN1C0
  --PORT MAP (
    --D   => tff_out,
    --CLK => del(length-1),
    --CLR => ena,
    --Q   => dff_out
  --);
--
  --inv_i : INV 
  --PORT MAP (
    --A => dff_out,
    --Y => tff_out
  --);
--
----tero_out <= tff_out;  -- modif nath
--tero_out <= dff_out;

END ARCHITECTURE rtl;
