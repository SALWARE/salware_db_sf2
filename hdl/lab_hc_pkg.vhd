---------------------------------------------------------------------
--
-- Design unit:   Package of constants for vhdl modules
--
-- File name:     lab_hc_pkg.vhd
--
-- Description:   This package defines common types, subtypes and
--                constants required to implement building blocks
--                of Hubert Curien laboratory designs in VHDL.
--                Important constants:
--                   - version number
--
-- System:        VHDL'93
--
-- Autor:         Viktor Fischer, Nathalie Bochard
--                Hubert Curien Laboratory, France
--
-- Copyright:     Hubert Curien Laboratory
--
-- Revision:      Version 2.00, January 2017
--
-- Last changes:  - ...
--
---------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

PACKAGE lab_hc_pkg IS         

--------------------------------------------------------------------------
-----   Type, sub-type and constant declarations for general use  --------
--------------------------------------------------------------------------
  SUBTYPE SLV_1_0   IS STD_LOGIC_VECTOR(1 DOWNTO 0);
  SUBTYPE SLV_2_0   IS STD_LOGIC_VECTOR(2 DOWNTO 0);
  SUBTYPE SLV_3_0   IS STD_LOGIC_VECTOR(3 DOWNTO 0);
  SUBTYPE SLV_4_0   IS STD_LOGIC_VECTOR(4 DOWNTO 0);
  SUBTYPE SLV_5_0   IS STD_LOGIC_VECTOR(5 DOWNTO 0);
  SUBTYPE SLV_6_0   IS STD_LOGIC_VECTOR(6 DOWNTO 0);
  SUBTYPE SLV_7_0   IS STD_LOGIC_VECTOR(7 DOWNTO 0);
  SUBTYPE SLV_8_0   IS STD_LOGIC_VECTOR(8 DOWNTO 0);
  SUBTYPE SLV_9_0   IS STD_LOGIC_VECTOR(9 DOWNTO 0);
  SUBTYPE SLV_11_0  IS STD_LOGIC_VECTOR(11 DOWNTO 0);
  SUBTYPE SLV_12_0  IS STD_LOGIC_VECTOR(12 DOWNTO 0);
  SUBTYPE SLV_13_0  IS STD_LOGIC_VECTOR(13 DOWNTO 0);
  SUBTYPE SLV_15_0  IS STD_LOGIC_VECTOR(15 DOWNTO 0);
  SUBTYPE SLV_16_0  IS STD_LOGIC_VECTOR(16 DOWNTO 0);
  SUBTYPE SLV_31_0  IS STD_LOGIC_VECTOR(31 DOWNTO 0);
  SUBTYPE SLV_127_0 IS STD_LOGIC_VECTOR(127 DOWNTO 0);

  ----------------------------------------------------------------------------------------
-- Config TERO_PUF
----------------------------------------------------------------------------------------
  CONSTANT SEL_WIDTH  : NATURAL := 6;                -- Data_select
  CONSTANT NRO_VAL    : NATURAL := 2**(SEL_WIDTH);   -- Number of tero cells
  CONSTANT WIDTH_MUX  : NATURAL := 2**(SEL_WIDTH);   -- Size data IN multiplexer
  CONSTANT NDE1_VAL   : NATURAL := 8;                -- Number of delay elements
  
END lab_hc_pkg;