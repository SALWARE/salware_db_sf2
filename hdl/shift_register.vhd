-- **********************************************************************************************
-- ** Copyright (c) Laboratoire Hubert Curien,  All rights reserved.                           **
-- **                                                                                          **
-- **                                                                                          **
-- ** The data exchange is covered by the HECTOR project NDA.                                  **
-- ** The data provided by us are for use within the HECTOR project only and are sole IP       **
-- ** of Laboratoire Hubert Curien. Any other use or distribution                              **
-- ** thereof has to be granted by us and otherwise will be in violation of the  project non   **
-- ** disclosure agreement.                                                                    **
-- **                                                                                          **
-- **********************************************************************************************
---------------------------------------------------------------------
--
-- Design unit:   Shift register
--
-- File name:     shift_register.vhd
--
-- Description:   Store the response bit of the TERO PUF one by one in a 128 bits register
--
-- Autor:         Brice Colombier, Ugo Mureddu, Hubert Curien Laboratory, France
--
-- Copyright:     Hubert Curien Laboratory
--
-- Revision:      Version 1.00, June 2016
--
---------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY shift_register IS

  GENERIC (
    width : INTEGER := 128);

  PORT (
    clk      : IN  STD_LOGIC;
    rst_n    : IN  STD_LOGIC;
    data_in  : IN  STD_LOGIC;
    ena      : IN  STD_LOGIC;
    data_out : OUT STD_LOGIC_VECTOR(width - 1 DOWNTO 0)
    );
END ENTITY shift_register;

ARCHITECTURE rtl OF shift_register IS

  SIGNAL data : STD_LOGIC_VECTOR(width - 1 DOWNTO 0) := (OTHERS => '0');

BEGIN  -- ARCHITECTURE rtl

  PROCESS (clk, rst_n) IS
  BEGIN  -- PROCESS
    IF rst_n = '0' THEN
      data <= (OTHERS => '0');
    ELSIF rising_edge(clk) THEN         -- rising clock edge
      IF ena = '1' THEN
        data <= data(width - 2 DOWNTO 0) & data_in;
      END IF;
    END IF;
  END PROCESS;

  data_out <= data;

END ARCHITECTURE rtl;
