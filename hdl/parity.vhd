LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY parity IS

  PORT (
    clk        : IN  STD_LOGIC;
    rst_n      : IN  STD_LOGIC;
    rst_n_sync : IN  STD_LOGIC;
    data       : IN  STD_LOGIC;
    ena        : IN  STD_LOGIC;
    par        : OUT STD_LOGIC);

END ENTITY parity;

ARCHITECTURE rtl OF parity IS

  SIGNAL par_sig : STD_LOGIC;

BEGIN  -- ARCHITECTURE rtl

  PROCESS (clk, rst_n) IS
  BEGIN  -- PROCESS
    IF rst_n = '0' THEN                 -- asynchronous reset (active low)
      par_sig <= '0';
    ELSIF rising_edge(clk) THEN         -- rising clock edge
      IF rst_n_sync = '0' THEN
        par_sig <= '0';
      ELSIF ena = '1' THEN
        par_sig <= par_sig XOR data;
      END IF;
    END IF;
  END PROCESS;

  par <= par_sig;

END ARCHITECTURE rtl;
