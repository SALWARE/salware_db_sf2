-- **********************************************************************************************
-- ** Copyright (c) Laboratoire Hubert Curien,  All rights reserved.                           **
-- **                                                                                          **
-- **                                                                                          **
-- ** The data exchange is covered by the HECTOR project NDA.                                  **
-- ** The data provided by us are for use within the HECTOR project only and are sole IP       **
-- ** of Laboratoire Hubert Curien. Any other use or distribution                              **
-- ** thereof has to be granted by us and otherwise will be in violation of the  project non   **
-- ** disclosure agreement.                                                                    **
-- **                                                                                          **
-- **********************************************************************************************
---------------------------------------------------------------------
--
-- Design unit:   TERO PUF core
--
-- File name:     TEROPUF_core.vhd
--
-- Description:   Core of the TERO composed of demux, tero cells, mux and comparator.
--
-- Autor:         Ugo Mureddu, Nathalie Bochard, Hubert Curien Laboratory, France
--
-- Copyright:     Hubert Curien Laboratory
--
-- Revision:      Version 1.00, June 2016
--
---------------------------------------------------------------------


LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY WORK;
USE WORK.lab_hc_pkg.ALL; 

library smartfusion2;
use smartfusion2.all;

ENTITY TEROPUF_core IS
  PORT
    (
      clk        : IN  STD_LOGIC;                                                 --global clk
      rst        : IN  STD_LOGIC := '1';                                          --reset counters
      ena        : IN  STD_LOGIC;                                                 --activate tero cells
      selec_in   : IN  STD_LOGIC_VECTOR(SEL_WIDTH-1 DOWNTO 0) := (OTHERS => '0'); --select tero cell
      output_puf : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);                              --comparison result
      strobe     : OUT STD_LOGIC                                                  --data ready signal
      );
END TEROPUF_core;

ARCHITECTURE rtl OF TEROPUF_core IS 

----------------------- 
-- Tero cell component --
-----------------------
  COMPONENT TERO_core IS
    GENERIC (
      length : INTEGER := NDE1_VAL
      );
    PORT(
      ctrl     : IN  STD_LOGIC;   -- Desables oscillator output
      tero_out : OUT STD_LOGIC    -- Oscillator output
      );
  END COMPONENT;


--------------------
--     CLKBUF     --
--------------------
COMPONENT CLKINT
    -- PORT LIST
    PORT(
        -- INPUTS
        A   : IN  STD_LOGIC;
        -- OUTPUTS
        Y   : OUT STD_LOGIC
        );
END COMPONENT;

--------------------------
-- multiplexor 128 to 1 --
--------------------------
  COMPONENT mux_tero IS

    GENERIC (
      WIDTH        : INTEGER := WIDTH_MUX;
      SELECT_WIDTH : INTEGER := SEL_WIDTH);

    PORT (
      data_in   : IN  STD_LOGIC_VECTOR((WIDTH-1) DOWNTO 0);
      selection : IN  STD_LOGIC_VECTOR((SELECT_WIDTH-1) DOWNTO 0);
      data_out  : OUT STD_LOGIC
		   );

  END COMPONENT;

----------------------------
-- demultiplexor 1 to 128 --
----------------------------
  COMPONENT demux IS

    GENERIC (
      WIDTH        : INTEGER := WIDTH_MUX;
      SELECT_WIDTH : INTEGER := SEL_WIDTH);

    PORT (
      data_in   : IN  STD_LOGIC;
      selection : IN  STD_LOGIC_VECTOR((SELECT_WIDTH-1) DOWNTO 0);
      data_out  : OUT STD_LOGIC_VECTOR((WIDTH-1) DOWNTO 0));

  END COMPONENT;
 
---------------------- 
-- Internal signals --
----------------------

  SIGNAL cnt1          : UNSIGNED(9 DOWNTO 0);  -- signal for storing number of rising edges of the first TERO
  SIGNAL cnt2          : UNSIGNED(9 DOWNTO 0);  -- signal for storing number of rising edges of the second TERO
  SIGNAL cnt1_done_int : STD_LOGIC;  -- signal indicating that the first counter has reached the value of 1024 rising edges (randomly choosen value!)
  SIGNAL cnt2_done_int : STD_LOGIC;  -- signal indicating that the second counter has reached the value of 1024 rising edges (randomly choosen value!)
  SIGNAL cnt_ctrl      : UNSIGNED (7 DOWNTO 0) := (OTHERS=>'0'); --signal activating ctrl for a defined period
  SIGNAL ctrl          : STD_LOGIC; -- activate the TERO cells
  SIGNAL mux1_out      : STD_LOGIC; -- TERO cell output muxed
  SIGNAL mux2_out      : STD_LOGIC; -- TERO cell output muxed
  SIGNAL mux1_out_buff : STD_LOGIC; -- TERO cell output muxed and buffered
  SIGNAL mux2_out_buff : STD_LOGIC; -- TERO cell output muxed and buffered
  SIGNAL tero_out_1    : STD_LOGIC_VECTOR((NRO_VAL - 1) DOWNTO 0); --TERO cell output
  SIGNAL teros_ctrl    : STD_LOGIC_VECTOR((NRO_VAL - 1) DOWNTO 0)   := (OTHERS => '0'); -- Desables oscillator output
  SIGNAL tero_out_2    : STD_LOGIC_VECTOR((NRO_VAL - 1) DOWNTO 0); --TERO cell output
  SIGNAL sub_result    : UNSIGNED(9 DOWNTO 0); -- difference between the two counter values
------------------------------
--    BEGIN ARCHITECTURE    --
------------------------------ 
BEGIN

------------------------------
--  Ring oscillator array   --
------------------------------
  tero_array_1 :
  FOR i IN 1 TO NRO_VAL GENERATE       
  BEGIN
    ring_n : TERO_core
      PORT MAP(
        ctrl     => teros_ctrl(i-1),       -- Desables oscillator output
        tero_out => tero_out_1(i-1));
  END GENERATE tero_array_1;
  
  tero_array_2 :
  FOR i IN 1 TO NRO_VAL GENERATE     
  BEGIN
    ring_n : TERO_core
      PORT MAP(
        ctrl     => teros_ctrl(i-1),       -- Desables oscillator output
        tero_out => tero_out_2(i-1));
  END GENERATE tero_array_2;
    
----------------------------------------------------
-- Switching TERO outputs using multiplexers      --
----------------------------------------------------  

  mux_out1_gen : mux_tero
    PORT MAP(
      data_in   => tero_out_1,
      selection => selec_in,
      data_out  => mux1_out);


  mux_out2_gen : mux_tero
    PORT MAP(
      data_in   => tero_out_2,
      selection => selec_in,
      data_out  => mux2_out);

  ena_gen : demux
    PORT MAP(
      data_in   => ctrl,
      selection => selec_in,
      data_out  => teros_ctrl
		);

------------------------------    
-- MUX1_OUT_BUFF
------------------------------
CLKBUF1 : CLKINT
    port map( 
        -- Inputs
        A   => mux1_out,
        -- Outputs
        Y   => mux1_out_buff 
        );

 ------------------------------    
-- MUX2_OUT_BUFF
------------------------------
CLKBUF2 : CLKINT
    port map( 
        -- Inputs
        A   => mux2_out,
        -- Outputs
        Y   => mux2_out_buff 
        );       

------------------------------------
-- Counters of the two TERO outputs --
------------------------------------

  PROCESS(mux1_out_buff, rst)  
  BEGIN
    IF (rst = '0') THEN
      cnt1 <= (OTHERS => '0');          -- reset of counters
    ELSIF rising_edge(mux1_out_buff) THEN
        IF ctrl = '0' OR cnt1_done_int = '1' OR cnt2_done_int = '1' THEN
          cnt1 <= cnt1;
        ELSE
          cnt1 <= cnt1 + 1; 
        END IF;
    END IF;
  END PROCESS;
--
  PROCESS(mux2_out_buff, rst)  -- counting process for the second counter
  BEGIN
    IF (rst = '0') THEN
      cnt2 <= (OTHERS => '0');         -- reset of counters
    ELSIF rising_edge(mux2_out_buff) THEN
        IF ctrl = '0' OR cnt1_done_int = '1' OR cnt2_done_int = '1' THEN
          cnt2 <= cnt2;
        ELSE
          cnt2 <= cnt2 + 1;
        END IF;
    END IF;
  END PROCESS;

---------------------------------------
-- Indication of the end of counting --
---------------------------------------

  cnt1_done_int <= '1' WHEN ((cnt1(9) = '1') AND (cnt2_done_int = '0')) ELSE '0';  -- if the first counter reaches the final value, this signal is set to one
  cnt2_done_int <= '1' WHEN ((cnt2(9) = '1') AND (cnt1_done_int = '0')) ELSE '0';  -- if the second counter reaches the final value, this signal is set to one

  ------------------------------------------------------
  -- generate ctrl signal for tero cells
  ------------------------------------------------------
-- 7-bits counter
  PROCESS(clk)
  BEGIN
    IF  rising_edge(clk) THEN
      IF ena='0' THEN
        cnt_ctrl <= (OTHERS=>'0');
        ctrl     <= '0';
		strobe   <= '0';
      ELSE
		IF cnt_ctrl = "00001000" THEN
			ctrl   <= '0';
			strobe <= '1';
            cnt_ctrl <= cnt_ctrl;
		ELSE
		    ctrl   <= '1';
            strobe <= '0';
            cnt_ctrl <= cnt_ctrl + 1;
		END IF;		
      END IF;
    END IF;
  END PROCESS;

  ---------------------------------------------------------------------------
  -- subtract the two counter value and send the MSB to the shift register
  ---------------------------------------------------------------------------
  sub_result <= cnt1 - cnt2;
  output_puf <= sub_result(9)&sub_result(5);
 
END rtl;