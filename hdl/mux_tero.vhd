-- **********************************************************************************************
-- ** Copyright (c) Laboratoire Hubert Curien,  All rights reserved.                           **
-- **                                                                                          **
-- **                                                                                          **
-- ** The data exchange is covered by the HECTOR project NDA.                                  **
-- ** The data provided by us are for use within the HECTOR project only and are sole IP       **
-- ** of Laboratoire Hubert Curien. Any other use or distribution                              **
-- ** thereof has to be granted by us and otherwise will be in violation of the  project non   **
-- ** disclosure agreement.                                                                    **
-- **                                                                                          **
-- **********************************************************************************************
---------------------------------------------------------------------
--
-- Design unit:   Multiplexor
--
-- File name:     mux_tero.vhd
--
-- Description:   Connect the oscillating TERO cells to the counters
--
-- Autor:         Ugo Mureddu, Hubert Curien Laboratory, France
--
-- Copyright:     Hubert Curien Laboratory
--
-- Revision:      Version 1.00, June 2016
--
---------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY WORK;
USE WORK.lab_hc_pkg.ALL;

ENTITY mux_tero IS

  GENERIC (
    WIDTH        : INTEGER := WIDTH_MUX;
    SELECT_WIDTH : INTEGER := SEL_WIDTH);

  PORT (
    data_in   : IN  STD_LOGIC_VECTOR(WIDTH - 1 DOWNTO 0);
    selection : IN  STD_LOGIC_VECTOR(SELECT_WIDTH - 1 DOWNTO 0);
    data_out  : OUT STD_LOGIC);

END ENTITY mux_tero;

ARCHITECTURE rtl OF mux_tero IS

BEGIN

  data_out <= data_in(to_integer(UNSIGNED(selection)));

END ARCHITECTURE rtl;
