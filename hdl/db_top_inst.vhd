-------------------------------------------------------------------------------
--
-- Design unit:   Daughter board top entity instance
--                 
--
-- File name:     db_top_inst.vhd
--
-- Description:   Top entity of daughter board instance, contains
--                of the IO pins instances.
-- 
--
-- System:        VHDL'93
--
-- Author:        Marek Laban, MICRONIC a.s., LabHC
--
-- Copyright:     MICRONIC a.s., LabHC
--
-- Created in frame of HECTOR project (No. 644052).
--
-- Revision:      Version 1.00, May 2016
-- History:       17/02/2017 - v1.00 Created (ML, MIC)
--
-------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

LIBRARY WORK;
USE WORK.lab_hc_pkg.ALL;

entity db_top_inst is
  PORT (
    -- pinout is named using the schematic names
    XTL         : IN  STD_LOGIC; -- 12MHz clock input 
	DATA0_P     : IN  STD_LOGIC; -- clk  (83 MHz)
    DATA0_N     : IN  STD_LOGIC; -- clk  (83 MHz)											 
    DATA3_P     : OUT STD_LOGIC; -- ssi_tx
    DATA3_N     : IN  STD_LOGIC; -- ssi_rx
    GPIO_1      : IN  STD_LOGIC; -- ssi_clk_i
    GPIO_2      : IN  STD_LOGIC; -- n_nst
    LED_OUT     : OUT STD_LOGIC  -- LED output
  );
end db_top_inst;

architecture architecture_db_top_inst of db_top_inst is
  -- daughter board instance
  COMPONENT puf_intfc IS
  PORT (
    clk          : IN  STD_LOGIC; -- input from quartz oscillator
    ssi_clk      : IN  STD_LOGIC; -- GPIO1
    n_reset      : IN  STD_LOGIC; -- GPIO2
    ssi_tx       : OUT STD_LOGIC; -- LVDS3 P
    ssi_rx       : IN  STD_LOGIC; -- LVDS3 N
    status       : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    led          : OUT STD_LOGIC;
    Data_from_DB : IN STD_LOGIC_VECTOR(1023 DOWNTO 0);
    data_rdy     : IN STD_LOGIC;
    start_process: OUT STD_LOGIC;
    puf_mode     : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    Data_to_DB   : OUT STD_LOGIC_VECTOR(1023 DOWNTO 0)
  );
  END COMPONENT;

  COMPONENT CLKINT IS
  PORT(
    A   : IN  STD_LOGIC;
    Y   : OUT STD_LOGIC
  );
  END COMPONENT;

  -- lvds input component
  COMPONENT lvds_in IS
  PORT(
    PADN_IN : IN  STD_LOGIC_VECTOR(0 TO 0);
    PADP_IN : IN  STD_LOGIC_VECTOR(0 TO 0);
    Y       : OUT STD_LOGIC_VECTOR(0 TO 0)
  );
  END COMPONENT LVDS_IN;

  -- lvds output component
  COMPONENT lvds_out IS
  PORT(
    D        : IN  STD_LOGIC_VECTOR(0 TO 0);
    PADN_OUT : OUT STD_LOGIC_VECTOR(0 TO 0);
    PADP_OUT : OUT STD_LOGIC_VECTOR(0 TO 0)
  );
  END COMPONENT lvds_out;

  -- output IO buffer
  COMPONENT out_buf IS
  PORT(
    D       : IN  STD_LOGIC_VECTOR(0 TO 0);
    PAD_OUT : OUT STD_LOGIC_VECTOR(0 TO 0)
  );
  END COMPONENT out_buf;

  -- input IO buffer
  COMPONENT in_buf IS  
  PORT(
    PAD_IN : IN  STD_LOGIC_VECTOR(0 TO 0);
    Y      : OUT STD_LOGIC_VECTOR(0 TO 0)
  );
  END COMPONENT in_buf;

  -- 12MHz external oscillator 
  COMPONENT ex_osc IS
  PORT(
    XTL        : IN  std_logic;
    XTLOSC_O2F : OUT std_logic
  );
  END COMPONENT ex_osc;

  -- elementary signals ------------------
  SIGNAL XTL_clk        : STD_LOGIC;
  SIGNAL rst            : STD_LOGIC;
  SIGNAL clk            : STD_LOGIC;
  SIGNAL clk_i          : STD_LOGIC;
  SIGNAL n_rst          : STD_LOGIC;
  --
  -- ssi interface signals
  SIGNAL ssi_clk  : STD_LOGIC; -- ssi signal from clk buffer to ssi comp.
  SIGNAL ssi_clk_i: STD_LOGIC; -- ssi signal from IO buffer to clk buffer
  SIGNAL ssi_rx   : STD_LOGIC; -- ssi rx daughter board signal
  SIGNAL ssi_tx   : STD_LOGIC; -- ssi tx daughter board signal
  --   
  SIGNAL led            : STD_LOGIC := '0';   -- led output
  
  --
  SIGNAL mux_sel : STD_LOGIC :='0'; -- multiplexor select (not used signal)
  
  -- PUF signals
  SIGNAL ena_PUF                     : STD_LOGIC;
  SIGNAL TERO_index                  : STD_LOGIC_VECTOR(6 DOWNTO 0);
  SIGNAL output_PUF                  : STD_LOGIC_VECTOR(1 DOWNTO 0);
  SIGNAL PUF_data_ready              : STD_LOGIC;
  SIGNAL rst_PUF                     : STD_LOGIC;

  -- Shift register signals
  SIGNAL ena_response_shift_register : STD_LOGIC;
  SIGNAL PUF_response                : STD_LOGIC_VECTOR(127 DOWNTO 0);

  --CASACDE signals
  SIGNAL PUF_response_bit            : STD_LOGIC;
  SIGNAL PUF_response_bit_index      : STD_LOGIC_VECTOR(6 DOWNTO 0);
  SIGNAL ena_parity_computation      : STD_LOGIC;
  SIGNAL ena_parity_shift_register   : STD_LOGIC;
  SIGNAL parity_value                : STD_LOGIC;
  SIGNAL rst_parity_register         : STD_LOGIC;
  SIGNAL parity_register_value       : STD_LOGIC_VECTOR(31 DOWNTO 0);
  SIGNAL rst_n_sync_parity           : STD_LOGIC;
  SIGNAL select_PUF_bit_index        : STD_LOGIC_VECTOR(6 DOWNTO 0);

  -- Sync signals between controller and comm interface
  SIGNAL Data_from_DB                : STD_LOGIC_VECTOR(1023 DOWNTO 0);
  SIGNAL data_rdy                    : STD_LOGIC;
  SIGNAL start_process               : STD_LOGIC;
  SIGNAL puf_mode                    : STD_LOGIC_VECTOR(7 DOWNTO 0);
  SIGNAL Data_to_DB                  : STD_LOGIC_VECTOR(1023 DOWNTO 0);
  SIGNAL status                      : STD_LOGIC_VECTOR(31 DOWNTO 0);

  --Design signals
  SIGNAL mult_out                    : STD_LOGIC_VECTOR(127 DOWNTO 0);

  -- Data to send
  SIGNAL select_data_to_send         : STD_LOGIC_VECTOR(1 DOWNTO 0);    

  --Activation signals
  SIGNAL AW_ready     : STD_LOGIC;
  SIGNAL AW_encrypted : STD_LOGIC_VECTOR(127 DOWNTO 0);
  SIGNAL key          : STD_LOGIC_VECTOR(127 DOWNTO 0);								  
BEGIN
  quartz : ex_osc PORT MAP(XTL => XTL, XTLOSC_O2F => XTL_clk);

  puf_intfc_inst : puf_intfc
  PORT MAP (
    clk           => clk,     -- input from quartz oscillator
    ssi_clk       => ssi_clk, -- GPIO1
    n_reset       => n_rst,   -- GPIO2
    ssi_tx        => ssi_tx,  -- LVDS3 P
    ssi_rx        => ssi_rx,  -- LVDS3 N
    led           => led,
    Data_from_DB  => Data_from_DB,
    data_rdy      => data_rdy,
    start_process => start_process,
    puf_mode      => puf_mode,  
    status        => status,
    Data_to_DB    => Data_to_DB
  ); 
 
  -- lvds input from motherboard -> clock 83 MHz
  lvds_in_clk_inst : lvds_in
  PORT MAP (
    PADN_IN(0) => DATA0_N,
    PADP_IN(0) => DATA0_P,
    Y(0)       => clk_i 
  );
 
  -- Entry to internal clock buffer
  CLKINT_2 : CLKINT
  PORT MAP(
    A   => clk_i,
    Y   => clk 
  );												

  -- input from motherboad -> negative reset 
  n_reset_inst : in_buf  
  PORT MAP (
    PAD_IN(0) => GPIO_2,
    Y(0)      => n_rst
  );  

  -- input from motherboad -> motherboard ssi TX - daughter board RX
  ssi_rx_inst : in_buf
  PORT MAP (
    PAD_IN(0) => DATA3_N,
    Y(0)      => ssi_rx
  );
  
  -- input from motherboad -> ssi clk
  ssi_clk_inst : in_buf
  PORT MAP (
    PAD_IN(0) => GPIO_1,
    Y(0)      => ssi_clk_i
  );

  -- Entry to internal clock buffer
  CLKINT_1 : CLKINT
  PORT MAP(
    A   => ssi_clk_i,
    Y   => ssi_clk 
  );

  -- output to motherboard -> motherboard ssi RX - daughter board TX
  ssi_out_inst : out_buf
  PORT MAP (
    D(0)       => ssi_tx,
    PAD_OUT(0) => DATA3_P
  );
  
  -- led output 
  led_out_inst : out_buf
  PORT MAP (
    D(0)       => led,
    PAD_OUT(0) => LED_OUT
  );

  TERO_PUF : ENTITY work.TEROPUF_core
  PORT MAP (
    clk        => XTL_clk,
    rst        => rst_PUF,
    ena        => ena_PUF,
    selec_in   => TERO_index(5 downto 0),
    output_puf => output_puf,
    strobe     => PUF_data_ready);

  response_shift_register : ENTITY work.shift_register_2_bits
    GENERIC MAP (
      width => 128)
    PORT MAP (
      clk      => XTL_clk,
      rst_n    => n_rst,
      data_in  => output_PUF,
      ena      => ena_response_shift_register,
      data_out => PUF_response);

  controller_1 : ENTITY work.controller
  PORT MAP (
    clk                         => XTL_clk,
    rst_n                       => n_rst,
    PUF_data_ready              => PUF_data_ready,
    rst_PUF                     => rst_PUF,
    ena_PUF                     => ena_PUF,
    TERO_index                  => TERO_index,
    ena_response_shift_register => ena_response_shift_register,
    select_PUF_bit_index        => select_PUF_bit_index,
    ena_parity_computation      => ena_parity_computation,
    rst_n_sync_parity_module    => rst_n_sync_parity,
    ena_parity_shift_register   => ena_parity_shift_register,
    rst_parity_register         => rst_parity_register,
    data_rdy                    => data_rdy,
    start_process               => start_process,
    status                      => status,
    nb_blocks                   => Data_to_DB(7 DOWNTO 0),
    select_data_to_send         => select_data_to_send,
    puf_mode                    => puf_mode,
	AW_ready                    => AW_ready);  

  mux_data_to_ssi : ENTITY work.mux3x128
    PORT MAP (
      data_in_0 => x"FFFFFFFFFFFFFFFFFFFFFFFF" & parity_register_value,
      data_in_1 => mult_out,
      data_in_2 => PUF_response,
      selection => select_data_to_send,
      data_out  => Data_from_DB(127 downto 0));

  mux_indexes : ENTITY work.mux7x896
    PORT MAP (
      data_in   => Data_to_DB(903 DOWNTO 8),
      selection => select_PUF_bit_index,
      data_out  => PUF_response_bit_index);

  mux_response_bits_to_parity : ENTITY work.mux_tero
    GENERIC MAP (
      WIDTH        => 128,
      SELECT_WIDTH => 7)
    PORT MAP (
      data_in   => PUF_response,
      selection => PUF_response_bit_index,
      data_out  => PUF_response_bit);

 parity_computation_module : ENTITY work.parity
    PORT MAP (
      clk        => XTL_clk,
      rst_n      => n_rst,
      rst_n_sync => rst_n_sync_parity,
      data       => PUF_response_bit,
      ena        => ena_parity_computation,
      par        => parity_value);

  parity_shift_register : ENTITY work.shift_register
    GENERIC MAP (
      width => 32)
    PORT MAP (
      clk      => XTL_clk,
      rst_n    => rst_parity_register,
      data_in  => parity_value,
      ena      => ena_parity_shift_register,
      data_out => parity_register_value);
	  
  multiplier_activable_1 : ENTITY work.multiplier_activable
    PORT MAP (
      a_0   => Data_to_DB(0),
      a_1   => Data_to_DB(1),
      a_2   => Data_to_DB(2),
      a_3   => Data_to_DB(3),
      a_4   => Data_to_DB(4),
      a_5   => Data_to_DB(5),
      a_6   => Data_to_DB(6),
      a_7   => Data_to_DB(7),
      a_8   => Data_to_DB(8),
      a_9   => Data_to_DB(9),
      a_10  => Data_to_DB(10),
      a_11  => Data_to_DB(11),
      a_12  => Data_to_DB(12),
      a_13  => Data_to_DB(13),
      a_14  => Data_to_DB(14),
      a_15  => Data_to_DB(15),
      a_16  => Data_to_DB(16),
      a_17  => Data_to_DB(17),
      a_18  => Data_to_DB(18),
      a_19  => Data_to_DB(19),
      a_20  => Data_to_DB(20),
      a_21  => Data_to_DB(21),
      a_22  => Data_to_DB(22),
      a_23  => Data_to_DB(23),
      a_24  => Data_to_DB(24),
      a_25  => Data_to_DB(25),
      a_26  => Data_to_DB(26),
      a_27  => Data_to_DB(27),
      a_28  => Data_to_DB(28),
      a_29  => Data_to_DB(29),
      a_30  => Data_to_DB(30),
      a_31  => Data_to_DB(31),
      a_32  => Data_to_DB(32),
      a_33  => Data_to_DB(33),
      a_34  => Data_to_DB(34),
      a_35  => Data_to_DB(35),
      a_36  => Data_to_DB(36),
      a_37  => Data_to_DB(37),
      a_38  => Data_to_DB(38),
      a_39  => Data_to_DB(39),
      a_40  => Data_to_DB(40),
      a_41  => Data_to_DB(41),
      a_42  => Data_to_DB(42),
      a_43  => Data_to_DB(43),
      a_44  => Data_to_DB(44),
      a_45  => Data_to_DB(45),
      a_46  => Data_to_DB(46),
      a_47  => Data_to_DB(47),
      a_48  => Data_to_DB(48),
      a_49  => Data_to_DB(49),
      a_50  => Data_to_DB(50),
      a_51  => Data_to_DB(51),
      a_52  => Data_to_DB(52),
      a_53  => Data_to_DB(53),
      a_54  => Data_to_DB(54),
      a_55  => Data_to_DB(55),
      a_56  => Data_to_DB(56),
      a_57  => Data_to_DB(57),
      a_58  => Data_to_DB(58),
      a_59  => Data_to_DB(59),
      a_60  => Data_to_DB(60),
      a_61  => Data_to_DB(61),
      a_62  => Data_to_DB(62),
      a_63  => Data_to_DB(63),
      b_0   => Data_to_DB(64),
      b_1   => Data_to_DB(65),
      b_2   => Data_to_DB(66),
      b_3   => Data_to_DB(67),
      b_4   => Data_to_DB(68),
      b_5   => Data_to_DB(69),
      b_6   => Data_to_DB(70),
      b_7   => Data_to_DB(71),
      b_8   => Data_to_DB(72),
      b_9   => Data_to_DB(73),
      b_10  => Data_to_DB(74),
      b_11  => Data_to_DB(75),
      b_12  => Data_to_DB(76),
      b_13  => Data_to_DB(77),
      b_14  => Data_to_DB(78),
      b_15  => Data_to_DB(79),
      b_16  => Data_to_DB(80),
      b_17  => Data_to_DB(81),
      b_18  => Data_to_DB(82),
      b_19  => Data_to_DB(83),
      b_20  => Data_to_DB(84),
      b_21  => Data_to_DB(85),
      b_22  => Data_to_DB(86),
      b_23  => Data_to_DB(87),
      b_24  => Data_to_DB(88),
      b_25  => Data_to_DB(89),
      b_26  => Data_to_DB(90),
      b_27  => Data_to_DB(91),
      b_28  => Data_to_DB(92),
      b_29  => Data_to_DB(93),
      b_30  => Data_to_DB(94),
      b_31  => Data_to_DB(95),
      b_32  => Data_to_DB(96),
      b_33  => Data_to_DB(97),
      b_34  => Data_to_DB(98),
      b_35  => Data_to_DB(99),
      b_36  => Data_to_DB(100),
      b_37  => Data_to_DB(101),
      b_38  => Data_to_DB(102),
      b_39  => Data_to_DB(103),
      b_40  => Data_to_DB(104),
      b_41  => Data_to_DB(105),
      b_42  => Data_to_DB(106),
      b_43  => Data_to_DB(107),
      b_44  => Data_to_DB(108),
      b_45  => Data_to_DB(109),
      b_46  => Data_to_DB(110),
      b_47  => Data_to_DB(111),
      b_48  => Data_to_DB(112),
      b_49  => Data_to_DB(113),
      b_50  => Data_to_DB(114),
      b_51  => Data_to_DB(115),
      b_52  => Data_to_DB(116),
      b_53  => Data_to_DB(117),
      b_54  => Data_to_DB(118),
      b_55  => Data_to_DB(119),
      b_56  => Data_to_DB(120),
      b_57  => Data_to_DB(121),
      b_58  => Data_to_DB(122),
      b_59  => Data_to_DB(123),
      b_60  => Data_to_DB(124),
      b_61  => Data_to_DB(125),
      b_62  => Data_to_DB(126),
      b_63  => Data_to_DB(127),
      f_0   => mult_out(0),
      f_1   => mult_out(1),
      f_2   => mult_out(2),
      f_3   => mult_out(3),
      f_4   => mult_out(4),
      f_5   => mult_out(5),
      f_6   => mult_out(6),
      f_7   => mult_out(7),
      f_8   => mult_out(8),
      f_9   => mult_out(9),
      f_10  => mult_out(10),
      f_11  => mult_out(11),
      f_12  => mult_out(12),
      f_13  => mult_out(13),
      f_14  => mult_out(14),
      f_15  => mult_out(15),
      f_16  => mult_out(16),
      f_17  => mult_out(17),
      f_18  => mult_out(18),
      f_19  => mult_out(19),
      f_20  => mult_out(20),
      f_21  => mult_out(21),
      f_22  => mult_out(22),
      f_23  => mult_out(23),
      f_24  => mult_out(24),
      f_25  => mult_out(25),
      f_26  => mult_out(26),
      f_27  => mult_out(27),
      f_28  => mult_out(28),
      f_29  => mult_out(29),
      f_30  => mult_out(30),
      f_31  => mult_out(31),
      f_32  => mult_out(32),
      f_33  => mult_out(33),
      f_34  => mult_out(34),
      f_35  => mult_out(35),
      f_36  => mult_out(36),
      f_37  => mult_out(37),
      f_38  => mult_out(38),
      f_39  => mult_out(39),
      f_40  => mult_out(40),
      f_41  => mult_out(41),
      f_42  => mult_out(42),
      f_43  => mult_out(43),
      f_44  => mult_out(44),
      f_45  => mult_out(45),
      f_46  => mult_out(46),
      f_47  => mult_out(47),
      f_48  => mult_out(48),
      f_49  => mult_out(49),
      f_50  => mult_out(50),
      f_51  => mult_out(51),
      f_52  => mult_out(52),
      f_53  => mult_out(53),
      f_54  => mult_out(54),
      f_55  => mult_out(55),
      f_56  => mult_out(56),
      f_57  => mult_out(57),
      f_58  => mult_out(58),
      f_59  => mult_out(59),
      f_60  => mult_out(60),
      f_61  => mult_out(61),
      f_62  => mult_out(62),
      f_63  => mult_out(63),
      f_64  => mult_out(64),
      f_65  => mult_out(65),
      f_66  => mult_out(66),
      f_67  => mult_out(67),
      f_68  => mult_out(68),
      f_69  => mult_out(69),
      f_70  => mult_out(70),
      f_71  => mult_out(71),
      f_72  => mult_out(72),
      f_73  => mult_out(73),
      f_74  => mult_out(74),
      f_75  => mult_out(75),
      f_76  => mult_out(76),
      f_77  => mult_out(77),
      f_78  => mult_out(78),
      f_79  => mult_out(79),
      f_80  => mult_out(80),
      f_81  => mult_out(81),
      f_82  => mult_out(82),
      f_83  => mult_out(83),
      f_84  => mult_out(84),
      f_85  => mult_out(85),
      f_86  => mult_out(86),
      f_87  => mult_out(87),
      f_88  => mult_out(88),
      f_89  => mult_out(89),
      f_90  => mult_out(90),
      f_91  => mult_out(91),
      f_92  => mult_out(92),
      f_93  => mult_out(93),
      f_94  => mult_out(94),
      f_95  => mult_out(95),
      f_96  => mult_out(96),
      f_97  => mult_out(97),
      f_98  => mult_out(98),
      f_99  => mult_out(99),
      f_100 => mult_out(100),
      f_101 => mult_out(101),
      f_102 => mult_out(102),
      f_103 => mult_out(103),
      f_104 => mult_out(104),
      f_105 => mult_out(105),
      f_106 => mult_out(106),
      f_107 => mult_out(107),
      f_108 => mult_out(108),
      f_109 => mult_out(109),
      f_110 => mult_out(110),
      f_111 => mult_out(111),
      f_112 => mult_out(112),
      f_113 => mult_out(113),
      f_114 => mult_out(114),
      f_115 => mult_out(115),
      f_116 => mult_out(116),
      f_117 => mult_out(117),
      f_118 => mult_out(118),
      f_119 => mult_out(119),
      f_120 => mult_out(120),
      f_121 => mult_out(121),
      f_122 => mult_out(122),
      f_123 => mult_out(123),
      f_124 => mult_out(124),
      f_125 => mult_out(125),
      f_126 => mult_out(126),
      f_127 => mult_out(127),
      key   => key);

  OTP_1 : ENTITY work.OTP
    GENERIC MAP (
      WIDTH => 128)
    PORT MAP (
      key    => PUF_response,
      input  => AW_encrypted,
      output => key);

  sampler_1 : ENTITY work.sampler
    GENERIC MAP (
      WIDTH => 128)
    PORT MAP (
      clk      => clk,
      rst_n    => n_rst,
      ena      => AW_ready,
      data_in  => Data_to_DB(127 DOWNTO 0),
      data_out => AW_encrypted);
END architecture_db_top_inst;
