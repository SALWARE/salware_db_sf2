--------------------------------------------------------------------------------
--
-- Design unit:   SSI TX communication submodule
--                 
--
-- File name:     ssi.vhd
--
-- Description:   Transmit data from TX submodule to RX submodule.
-- 
--
-- System:        VHDL'93
--
-- Author:        Oto Petura LabHC
--
-- Copyright:     MICRONIC a.s., LabHC
--  
-- Created in frame of HECTOR project (No. 644052).
--
-- Revision:      Version v5.00, February 2017
-- History:       17/02/2017 - v5.00 Sctructure modified (ML, MIC)
--
--------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY ssi_tx IS
  GENERIC (
    BSIZE    : INTEGER := 64          -- Data bus width MAX=511)
  );
  PORT (
    reset    : IN  STD_LOGIC;         -- reset
    clk      : IN  STD_LOGIC;         -- clock
    --
    tx       : OUT STD_LOGIC;         -- output TX line
    data_tx  : IN  STD_LOGIC_VECTOR ( BSIZE-1 DOWNTO 0 ); -- TX-ed data
    --
    tx_iprg  : OUT STD_LOGIC;         -- tx in progress flag
    start    : IN  STD_LOGIC          -- transmitting start signal 
  );
END ssi_tx;


ARCHITECTURE ssi_tx_arch OF ssi_tx IS

  TYPE   ssi_tx_states IS (IDLE, TX_START, TRANSMIT, TX_STOP);
  SIGNAL state : ssi_tx_states;
  
  -- received bit counter
  SIGNAL bit_cnt : UNSIGNED ( 8 DOWNTO 0 );
  -- shift register used for transmitting data  
  SIGNAL shreg   : STD_LOGIC_VECTOR ( BSIZE-1 DOWNTO 0 );
  
BEGIN

-- TX state machine
  PROCESS(reset, clk)
  BEGIN
    IF ( reset = '1' ) THEN
      state     <= IDLE;
      bit_cnt   <= (OTHERS => '0');
      shreg     <= (OTHERS => '0');
    ELSIF rising_edge(clk) THEN
      CASE state IS
        WHEN IDLE =>
          IF (start = '1') THEN  -- when start signal asserted
            state <= TX_START;   
            shreg <= data_tx;    -- load TX-ed word
          END IF;
        
        -- inserts start bit to tx line
        WHEN TX_START =>
          state   <= TRANSMIT;
        
        -- transmit BSIZE bits to slave device
        WHEN TRANSMIT =>
          -- shift data to output TX line
          shreg(BSIZE-2 DOWNTO 0) <= shreg(BSIZE-1 DOWNTO 1);
          -- send BSIZE - 1 bits 
          IF ( bit_cnt < BSIZE-1 ) THEN
            bit_cnt   <= bit_cnt + 1;
          -- when last bit is sent
          ELSE
            bit_cnt  <= (OTHERS => '0');  -- reset counter
            state    <= TX_STOP;          -- send stop bit
          END IF;
        
        -- send stop bit   
        WHEN TX_STOP =>
          state <= IDLE;
      END CASE;
    END IF;
  END PROCESS;
  
  -- transmission in progress signal
  tx_iprg <= '1'  WHEN (state /= IDLE) ELSE '0';
  
  -- TX line 
  tx <= '1'      WHEN (state = IDLE OR state = TX_STOP) ELSE -- stop bit
        '0'      WHEN (state = TX_START               ) ELSE -- start bit
        shreg(0) WHEN (state = TRANSMIT               );     -- data bit

END ssi_tx_arch;
