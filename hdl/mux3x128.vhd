---------------------------------------------------------------------
--
-- Design unit:   Multiplexor
--
-- File name:     mux.vhd
--
-- Description:   Connect the oscillating RO cells to the counters
--
-- Autor:         Ugo Mureddu, Hubert Curien Laboratory, France
--
-- Copyright:     Hubert Curien Laboratory
--
-- Revision:      Version 1.00, June 2016
--
---------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY mux3x128 IS

  PORT (
    data_in_0 : IN  STD_LOGIC_VECTOR(127 DOWNTO 0);
    data_in_1 : IN  STD_LOGIC_VECTOR(127 DOWNTO 0);
    data_in_2 : IN  STD_LOGIC_VECTOR(127 DOWNTO 0);
    selection : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
    data_out  : OUT STD_LOGIC_VECTOR(127 DOWNTO 0));

END ENTITY mux3x128;

ARCHITECTURE rtl OF mux3x128 IS

BEGIN  -- ARCHITECTURE rtl

  WITH selection SELECT
    data_out <=
    data_in_0 WHEN "00",
    data_in_1 WHEN "01",
    data_in_2 WHEN OTHERS;

END ARCHITECTURE rtl;
