LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY sampler IS

  GENERIC (
    WIDTH : INTEGER);

  PORT (
    clk      : IN  STD_LOGIC;
    rst_n    : IN  STD_LOGIC;
    ena      : IN  STD_LOGIC;
    data_in  : IN  STD_LOGIC_VECTOR(WIDTH - 1 DOWNTO 0);
    data_out : OUT STD_LOGIC_VECTOR(WIDTH - 1 DOWNTO 0));

END ENTITY sampler;

ARCHITECTURE rtl OF sampler IS

  SIGNAL data : STD_LOGIC_VECTOR(WIDTH - 1 DOWNTO 0);

BEGIN  -- ARCHITECTURE rtl

  PROCESS (clk, rst_n) IS
  BEGIN  -- PROCESS
    IF rst_n = '0' THEN                 -- asynchronous reset (active low)
      data <= (OTHERS => '0');
    ELSIF rising_edge(clk) THEN         -- rising clock edge
      IF ena = '1' THEN
        data <= data_in;
      END IF;
    END IF;
  END PROCESS;

  data_out <= data;

END ARCHITECTURE rtl;
