--------------------------------------------------------------------------------
--
-- Design unit:   SSI RX communication submodule
--                 
--
-- File name:     ssi.vhd
--
-- Description:   Receive data from TX submodule.
-- 
--
-- System:        VHDL'93
--
-- Author:        Oto Petura LabHC
--
-- Copyright:     MICRONIC a.s., LabHC
--  
-- Created in frame of HECTOR project (No. 644052).
--
-- Revision:      Version v5.00, February 2017
-- History:       17/02/2017 - v5.00 Sctructure modified (ML, MIC)
--
--------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY ssi_rx IS
  GENERIC (
    BSIZE     : INTEGER := 64         -- Data bus width (MAX=511)
  );
  PORT (
    reset    : IN  STD_LOGIC;         -- reset
    clk      : IN  STD_LOGIC;         -- clock
    --
    rx       : IN  STD_LOGIC;         -- input RX line
    data_rx  : OUT STD_LOGIC_VECTOR ( BSIZE-1 DOWNTO 0 ); -- parallel RX-ed data
    --
    rx_iprg  : OUT STD_LOGIC;         -- receiving in progress
    ready    : OUT STD_LOGIC          -- data ready on data_rx output
  );
END ssi_rx;

ARCHITECTURE ssi_rx_arch OF ssi_rx IS

  TYPE ssi_rx_states IS (WAIT_START, RECEIVE, WAIT_STOP);
  SIGNAL state : ssi_rx_states;
  
  -- received bit counter
  SIGNAL bit_cnt    : UNSIGNED        (8 DOWNTO 0);  
  -- shift register used for receiving data
  SIGNAL shreg      : STD_LOGIC_VECTOR(BSIZE - 1 DOWNTO 0);
  -- output latch data register
  SIGNAL data_tx_s  : STD_LOGIC_VECTOR(BSIZE - 1 DOWNTO 0);
  
BEGIN

  -- RX state machine
  PROCESS( reset, clk )
  BEGIN
    IF ( reset = '1' ) THEN
      state    <= WAIT_START;
      data_rx  <= (OTHERS => '0');
      bit_cnt  <= (OTHERS => '0');
      ready    <= '0';
    ELSIF falling_edge( clk ) THEN
      CASE state IS
        WHEN WAIT_START =>              
          IF ( rx = '0' ) THEN          -- wait for start bit
            ready          <= '0';      -- reset output data valid signal
            state          <= RECEIVE;  -- start to receiving bits
          END IF;
          
        WHEN RECEIVE =>
          -- receive BSIZE-1 bits
          IF ( bit_cnt < BSIZE-1 ) THEN
            shreg(BSIZE-1 DOWNTO 0) <= rx & shreg(BSIZE-1 DOWNTO 1);
            bit_cnt                 <= bit_cnt + 1;
          --  
          -- when last bit is received 
          ELSE
            data_rx   <= rx & shreg(BSIZE-1 DOWNTO 1); -- set data to output 
            bit_cnt   <= (OTHERS => '0');              -- reset counter of bits
            state     <= WAIT_STOP;                    
          END IF;
          --
        WHEN WAIT_STOP =>
          IF ( rx = '1' ) THEN         -- wait for stop bit
            ready     <= '1';          -- set ready signal
            state     <= WAIT_START;  
          END IF;
      END CASE;
    END IF;
  END PROCESS;
  
  -- receiving in progress
  rx_iprg <= '1'  WHEN (state = RECEIVE OR state = WAIT_STOP) ELSE '0';
  
END ssi_rx_arch;

