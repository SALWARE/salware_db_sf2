--------------------------------------------------------------------------------
--
-- Design unit:   Daughter board PUF interface
--                 
--
-- File name:     puf_intfc.vhd
--
-- Description:   Example how to load, process and send data back to motherboard
--                using the SSI interface.
--                Data are shifted to register to the LSB bit side, e.g.
--                |1|0|0|1|0| <-  10010
--                and shifted back, from the LSB side again, e.g.
--                |1|0|0|1|0| ->  10010
-- 
--
-- System:        VHDL'93
--
-- Author:        Marek Laban, MICRONIC a.s., LabHC
--
-- Copyright:     MICRONIC a.s., LabHC
--
-- Created in frame of HECTOR project (No. 644052).
--
-- Revision:      Version 1.00, May 2016
-- History:       02/03/2017 - v1.00 Created (ML, MIC)
--
-------------------------------------------------------------------------------

LIBRARY ieee;

USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.lab_hc_pkg.ALL;

ENTITY puf_intfc IS
  PORT (
    clk          : IN  STD_LOGIC; -- input from quartz oscillator
    ssi_clk      : IN  STD_LOGIC; -- GPIO1
    n_reset      : IN  STD_LOGIC; -- GPIO2
    ssi_tx       : OUT STD_LOGIC; -- LVDS3 P
    ssi_rx       : IN  STD_LOGIC; -- LVDS3 N
    status       : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    Data_from_DB : IN  STD_LOGIC_VECTOR(1023 DOWNTO 0);
    data_rdy     : IN  STD_LOGIC;
    led          : OUT STD_LOGIC;
    start_process: OUT STD_LOGIC;
    puf_mode     : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    Data_to_DB   : OUT STD_LOGIC_VECTOR(1023 DOWNTO 0)
  );
END puf_intfc; 

ARCHITECTURE arch OF puf_intfc IS
 
  COMPONENT ssi IS
  GENERIC (
    BSIZE    : INTEGER := 64    -- Data bus BSIZE (MAX=511)
  );
  PORT (
    reset    : IN  STD_LOGIC;   -- Positive reset
    ssi_clk  : IN  STD_LOGIC;   -- ssi clocks 
    clk      : IN  STD_LOGIC;   -- Internal clocks(3 times faster than ssi_clk)
    --
    -- data input/output from device
    rx       : IN  STD_LOGIC;   -- RX serial line
    tx       : OUT STD_LOGIC;   -- TX serial line
    --
    -- tx side ------------------------------------------  
    -- Parallel data input
    data_tx  : IN  STD_LOGIC_VECTOR ( BSIZE-1 DOWNTO 0 ); 
    tx_iprg  : OUT STD_LOGIC;   -- TX in progress
    start    : IN  STD_LOGIC;   -- when asserted, data are transmitted by ssi
    --
    -- rx side ------------------------------------------
    -- Parallel data output
    data_rx  : OUT STD_LOGIC_VECTOR ( BSIZE-1 DOWNTO 0 );
    data_rdy : OUT STD_LOGIC    -- RX data ready
  );  
  END COMPONENT ssi;  

  -- positive reset and clocks 
  SIGNAL reset          : STD_LOGIC;
  
  TYPE ssi_states IS (WAIT_CTRL, RECEIVE_DATA, PROCESS_DATA, SEND_DATA, 
                      SEND_STATUS);
  SIGNAL ssi_state     : ssi_states := WAIT_CTRL;
  
  -- data vector used for receiving and sending data
  SIGNAL my_data: STD_LOGIC_VECTOR(1023 DOWNTO 0);
  -- number of sent words  
  SIGNAL db2mb_nmb      : UNSIGNED(7 DOWNTO 0);
  -- number of received words
  SIGNAL mb2db_nmb      : UNSIGNED(7 DOWNTO 0);
  -- type of the processing
  SIGNAL puf_mode_reg   : STD_LOGIC_VECTOR(7 DOWNTO 0);
  -- ssi start signal - assert high when you want to send data
  SIGNAL ssi_data_start : STD_LOGIC;
  -- ssi tx in progress signal - asserted high during transmission
  SIGNAL ssi_tx_iprg    : STD_LOGIC;
  -- ssi output data tx vector
  SIGNAL ssi_data_tx    : STD_LOGIC_VECTOR(63 DOWNTO 0);
  -- ssi input data rx vector
  SIGNAL ssi_data_rx    : STD_LOGIC_VECTOR(63 DOWNTO 0);
  -- ssi data ready signal - asserted high when data are ready on the output 
  SIGNAL ssi_data_rdy   : STD_LOGIC;
  -- led output signal
  SIGNAL my_led         : STD_LOGIC;
  -- status of the daughter board
  SIGNAL status_reg     : STD_LOGIC_VECTOR(31 DOWNTO 0);
  -- data ready flag from FSM
  SIGNAL data_rdy_reg   : STD_LOGIC;
BEGIN

  -- Example of data transmission
  PROCESS(clk, reset)
  BEGIN
    IF ( reset = '1' ) THEN
      ssi_state        <= WAIT_CTRL;
      db2mb_nmb        <= (OTHERS => '0');
      mb2db_nmb        <= (OTHERS => '0');
      puf_mode_reg     <= (OTHERS => '0');
      my_data          <= (OTHERS => '0');
      ssi_data_tx      <= (OTHERS => '0');
      status_reg       <= (OTHERS => '0');
      ssi_data_start   <= '0';
      my_led           <= '1';
	  data_rdy_reg     <= '0';
    ELSIF rising_edge(clk) THEN
      data_rdy_reg <= data_rdy;
      status_reg   <= status;
      CASE ssi_state IS
        -- wait for the control data word
        WHEN WAIT_CTRL =>
          IF (ssi_data_rdy = '1') THEN
            -- save number of sent words
            db2mb_nmb   <= UNSIGNED(ssi_data_rx(7  DOWNTO 0));
            -- save number of received words
            mb2db_nmb   <= UNSIGNED(ssi_data_rx(15 DOWNTO 8));
            -- save mode of processing
            puf_mode_reg<= ssi_data_rx(23 DOWNTO 16);
            -- change state of led
			my_led      <= NOT my_led;
            ssi_state   <= RECEIVE_DATA;
          ELSE
            ssi_state   <= WAIT_CTRL;
          END IF;

        -- receive data (mb2db_nmb x 64bits)
        WHEN RECEIVE_DATA =>
          -- check how many words are needed
          IF (mb2db_nmb /= X"00") THEN
            -- when data are available -> save to register
            IF(ssi_data_rdy = '1')THEN
              my_data     <= my_data(959 DOWNTO 0) & ssi_data_rx;
              mb2db_nmb   <= mb2db_nmb - 1;
            END IF;
          ELSE
            ssi_state <= PROCESS_DATA;
          END IF;
        -- little modification of the input vector

        WHEN PROCESS_DATA =>
          Data_to_DB   <= my_data;
          start_process <= '1';
          IF(data_rdy_reg = '0')THEN
             ssi_state   <= PROCESS_DATA;
          ELSE
            my_data       <= Data_from_DB;
            start_process <= '0';
            -- change state of led
            
            ssi_state     <= SEND_STATUS;
          END IF;
       
        -- just send status of the daughter board
        WHEN SEND_STATUS =>          
          IF(ssi_tx_iprg = '0')THEN
            -- send status
            ssi_data_tx(31 DOWNTO  0) <= status_reg;
            ssi_data_tx(63 DOWNTO 32) <= (OTHERS => '0');
            -- start transfer              
            ssi_data_start <= '1';
            ssi_state      <= SEND_DATA;              
          ELSE
            ssi_data_start <= '0';
          END IF;
          
        -- send data (db2mb_nmb x 64bits)  
        WHEN SEND_DATA =>
          -- check how many words is need to send
          IF (db2mb_nmb /= X"00") THEN
            -- when ssi tx is not in progress -> insert data
            IF(ssi_tx_iprg = '0')THEN
              -- send data by shifting from LSB side
              ssi_data_tx           <= my_data(63   DOWNTO  0);
              my_data(959 DOWNTO 0) <= my_data(1023 DOWNTO 64);
              -- start transfer              
              ssi_data_start <= '1';
              db2mb_nmb      <= db2mb_nmb - 1;
            ELSE
              ssi_data_start <= '0';
            END IF;
          ELSE
            -- when no data to transfer -> wait for control word again
            ssi_data_start <= '0';          
            ssi_state      <= WAIT_CTRL;
          END IF;
        WHEN OTHERS => 
          ssi_state   <= WAIT_CTRL;
      END CASE;
    END IF;
  END PROCESS;

-- SSI interface - used for transferring data from and to daughter board -------
  ssi_slave_inst : ssi
    GENERIC MAP (
      BSIZE   => 64
    )
    PORT MAP (
      reset    => reset,
      clk      => clk,
      ssi_clk  => ssi_clk,

      -- slave device interface
      rx       => ssi_rx,
      tx       => ssi_tx,
      
      -- tx side of instance    
      start    => ssi_data_start,
      tx_iprg  => ssi_tx_iprg,
      data_tx  => ssi_data_tx,

      -- rx side of interface 
      data_rx  => ssi_data_rx,
      data_rdy => ssi_data_rdy
    );           

  reset     <= (NOT n_reset);     
  led       <= my_led;
  puf_mode  <= puf_mode_reg;
END ARCHITECTURE arch;
