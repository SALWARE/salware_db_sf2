-- **********************************************************************************************
-- ** Copyright (c) Laboratoire Hubert Curien,  All rights reserved.                           **
-- **                                                                                          **
-- **                                                                                          **
-- ** The data exchange is covered by the HECTOR project NDA.                                  **
-- ** The data provided by us are for use within the HECTOR project only and are sole IP       **
-- ** of Laboratoire Hubert Curien. Any other use or distribution                              **
-- ** thereof has to be granted by us and otherwise will be in violation of the  project non   **
-- ** disclosure agreement.                                                                    **
-- **                                                                                          **
-- **********************************************************************************************
---------------------------------------------------------------------
--
-- Design unit:   Buffer wrapper
--
-- File name:     buff_wrp.vhd
--
-- Description:   This file wraps the buffer composing the TERO cell. It allows to switch from one family to another easily.
--
-- Autor:         Ugo Mureddu, Hubert Curien Laboratory, France
--
-- Copyright:     Hubert Curien Laboratory
--
-- Revision:      Version 1.00, June 2016
--
---------------------------------------------------------------------
-- 
--
-- Family: Microsemi SmartFusion 2

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

LIBRARY smartfusion2;
USE smartfusion2.ALL;

ENTITY buff_wrp IS
  PORT (
    i : IN  STD_LOGIC;
    o : OUT STD_LOGIC
  );
END ENTITY buff_wrp;

ARCHITECTURE buff_wrp_arch OF buff_wrp IS

    COMPONENT and2 IS 
    PORT 
    ( 
      a : IN  STD_LOGIC := '1';
      b : IN  STD_LOGIC := '1';
      y : OUT STD_LOGIC
    ); 
  END COMPONENT and2;
  
BEGIN

  buff_wrp_i : and2
    PORT MAP
    ( 
      a => i,
      b => i,
      y  => o
    );

END buff_wrp_arch;
