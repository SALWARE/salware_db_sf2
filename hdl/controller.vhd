LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY controller IS

  PORT (
    clk                         : IN  STD_LOGIC;
    rst_n                       : IN  STD_LOGIC;
    PUF_data_ready              : IN  STD_LOGIC;
    start_process               : IN  STD_LOGIC;
    puf_mode                    : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
    nb_blocks                   : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
    rst_PUF                     : OUT STD_LOGIC;
    ena_PUF                     : OUT STD_LOGIC;
    TERO_index                  : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    ena_response_shift_register : OUT STD_LOGIC;
    ena_parity_computation      : OUT STD_LOGIC;
    rst_n_sync_parity_module    : OUT STD_LOGIC;
    ena_parity_shift_register   : OUT STD_LOGIC;
    rst_parity_register         : OUT STD_LOGIC;
    select_data_to_send         : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    select_PUF_bit_index        : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    status                      : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    data_rdy                    : OUT STD_LOGIC;
	AW_ready                    : OUT STD_LOGIC
    );

END ENTITY controller;

ARCHITECTURE behavioural OF controller IS

  TYPE state_type IS (IDLE,
                      WAIT_FOR_NEW_COMMAND,
                      DATA_PROCESSED_PUF,
                      DATA_PROCESSED_CASCADE,
                      DATA_PROCESSED_COMB,

                      GEN_RESPONSE_BIT_PUF,
                      STORE_RESPONSE_BIT_PUF,
                      RESET_PUF_COUNTERS,

                      CASCADE_MODE,
                      SELECT_PARITY_BIT,
                      ENABLE_PARITY,
                      COMPUTE_PARITY,
                      STORE_PARITY_BIT,
                      RESET_PARITY_MODULE,

                      START_PROCESSING,
					  
					  RECEIVE_AW,
                      DATA_PROCESSED_AW

                      );
  SIGNAL state : state_type;

  SIGNAL counter                 : UNSIGNED(6 DOWNTO 0);
  SIGNAL block_size              : UNSIGNED(6 DOWNTO 0);
  SIGNAL nb_blocks_reg           : UNSIGNED(7 DOWNTO 0);
  SIGNAL nb_indexes_processed    : UNSIGNED(6 DOWNTO 0);
  SIGNAL select_PUF_bit_index_reg: UNSIGNED(6 DOWNTO 0);
  --synchro FSM input
  SIGNAL PUF_data_ready_reg      : STD_LOGIC;
  SIGNAL puf_mode_reg            : STD_LOGIC_VECTOR(7 DOWNTO 0);
  SIGNAL start_process_reg       : STD_LOGIC;

BEGIN

  -- purpose: Synchronise the inputs
  PROCESS (clk, rst_n) IS
  BEGIN  -- PROCESS
    IF rst_n = '0' THEN                 -- asynchronous reset (active low)
      PUF_data_ready_reg      <= '0';
      puf_mode_reg            <= (OTHERS => '0');
      start_process_reg       <= '0';
    ELSIF rising_edge(clk) THEN         -- rising clock edge
      PUF_data_ready_reg      <= PUF_data_ready;
      puf_mode_reg            <= puf_mode;
      start_process_reg       <= start_process;
    END IF;
  END PROCESS;

  -- purpose: Compute the next state
  PROCESS (clk, rst_n) IS
  BEGIN  -- PROCESS
    IF rst_n = '0' THEN                 -- asynchronous reset (active low)
      state                    <= IDLE;
      select_data_to_send      <= "00";
      counter                  <= (OTHERS => '0');
      nb_blocks_reg            <= (OTHERS => '0');
      block_size               <= (OTHERS => '0');
      nb_indexes_processed     <= (OTHERS => '0');
      select_PUF_bit_index_reg <= (OTHERS => '0');
      data_rdy <= '0';
    ELSIF rising_edge(clk) THEN         -- rising clock edge
      CASE state IS
        WHEN IDLE =>
          counter <= (OTHERS => '0');
          data_rdy <= '0';
          IF start_process_reg = '1' THEN
            CASE puf_mode_reg(3 DOWNTO 0) IS
              WHEN X"1" =>              -- 1: Generate response with counter
                state <= GEN_RESPONSE_BIT_PUF;
              WHEN X"2" =>              -- 2: CASCADE mode
                state <= CASCADE_MODE;
              WHEN X"3" =>              -- 3: Send desing data out
                state <= START_PROCESSING;
              WHEN X"4" =>              -- 4: Receive activation word
                state <= RECEIVE_AW;							 
              WHEN OTHERS =>
                status   <= X"B00BADEF";
                data_rdy <= '1';
                state    <= IDLE;
            END CASE;
          END IF;

        ------------------------------------------------------
        -- 1: Generate response with counter as challenge
        ------------------------------------------------------   
        WHEN GEN_RESPONSE_BIT_PUF =>
          IF PUF_data_ready_reg = '1' THEN
            state <= STORE_RESPONSE_BIT_PUF;
          END IF;

        WHEN STORE_RESPONSE_BIT_PUF =>
          counter <= counter + 1;
          state   <= RESET_PUF_COUNTERS;

        WHEN RESET_PUF_COUNTERS =>
          IF counter = "1000000" THEN   --back to zero
            state <= DATA_PROCESSED_PUF;
          ELSE
            state <= GEN_RESPONSE_BIT_PUF;
          END IF;

        ------------------
        -- 2: CASCADE mode
        ------------------
        WHEN CASCADE_MODE =>
          state                <= SELECT_PARITY_BIT;
          select_PUF_bit_index_reg <= "0000000";
          nb_indexes_processed     <= (OTHERS => '0');
          nb_blocks_reg            <= unsigned(nb_blocks);
          CASE puf_mode_reg(7 DOWNTO 4) IS
            WHEN x"1" =>
              block_size <= "0000001";  -- 1
            WHEN x"2" =>
              block_size <= "0000010";  -- 2
            WHEN x"3" =>
              block_size <= "0000100";  -- 4
            WHEN x"4" =>
              block_size <= "0001000";  -- 8
            WHEN x"5" =>
              block_size <= "0010000";  -- 16
            WHEN x"6" =>
              block_size <= "0100000";  -- 32
            WHEN x"7" =>
              block_size <= "1000000";  -- 64
            WHEN OTHERS =>
              state <= WAIT_FOR_NEW_COMMAND;
          END CASE;

        WHEN SELECT_PARITY_BIT =>
            state <= ENABLE_PARITY;

        WHEN ENABLE_PARITY =>
          nb_indexes_processed <= nb_indexes_processed + 1;
          state                <= COMPUTE_PARITY;

        WHEN COMPUTE_PARITY =>
          select_PUF_bit_index_reg <= select_PUF_bit_index_reg + 1;
          IF nb_indexes_processed = block_size THEN
            state         <= STORE_PARITY_BIT;
            nb_blocks_reg <= nb_blocks_reg - 1;
          ELSE
            state <= SELECT_PARITY_BIT;
          END IF;

        WHEN STORE_PARITY_BIT =>
          nb_indexes_processed <= (OTHERS => '0');
          state                <= RESET_PARITY_MODULE;

        WHEN RESET_PARITY_MODULE =>
          IF nb_blocks_reg = 0 THEN
            state <= DATA_PROCESSED_CASCADE;
          ELSE
            state <= SELECT_PARITY_BIT;
          END IF;

        ------------------
        -- 3: START PROCESSING
        ------------------
        WHEN START_PROCESSING =>
            state <= DATA_PROCESSED_COMB;
        -----------------------------
        -- 4: RECEIVE ACTIVATION WORD
        -----------------------------
        
        WHEN RECEIVE_AW =>
            state <= DATA_PROCESSED_AW;		

        ----------------------------------------------------
        -- Buffer states to avoid considering a command twice
        ----------------------------------------------------

        WHEN DATA_PROCESSED_PUF =>
          data_rdy            <= '1';
          select_data_to_send <= "10";
          status              <= X"CAFEBABE";
          state               <= WAIT_FOR_NEW_COMMAND;

        WHEN DATA_PROCESSED_CASCADE =>
          data_rdy            <= '1';
          select_data_to_send <= "00";
          status              <= X"CA5CADED";
          state               <= WAIT_FOR_NEW_COMMAND;

        WHEN DATA_PROCESSED_COMB =>
          data_rdy            <= '1';
          select_data_to_send <= "01";
          status              <= X"B2092151";
          state               <= WAIT_FOR_NEW_COMMAND;

		WHEN DATA_PROCESSED_AW =>
          data_rdy            <= '1';
          status              <= X"AC7EEEEF";
          state               <= WAIT_FOR_NEW_COMMAND;
											  
        WHEN WAIT_FOR_NEW_COMMAND =>
          IF start_process_reg = '0' THEN
            state <= IDLE;
          END IF;

        WHEN OTHERS =>
            status   <= X"DEFEC8ED";
            data_rdy <= '1';
            state    <= IDLE;

      END CASE;
    END IF;
  END PROCESS;

  ---------------------
  -- Decode the outputs
  ---------------------
  rst_PUF <= '0' WHEN state = RESET_PUF_COUNTERS ELSE
             '0' WHEN state = IDLE ELSE
             '0' WHEN state = WAIT_FOR_NEW_COMMAND ELSE
             '1';

  ena_PUF                     <= '1' WHEN state = GEN_RESPONSE_BIT_PUF   ELSE '0';

  ena_response_shift_register <= '1' WHEN state = STORE_RESPONSE_BIT_PUF ELSE '0';

  TERO_index                  <= STD_LOGIC_VECTOR(counter);

  rst_parity_register <= '0' WHEN state = CASCADE_MODE         ELSE
                         '0' WHEN state = GEN_RESPONSE_BIT_PUF ELSE
                         '0' WHEN state = START_PROCESSING     ELSE
                         '1';

  ena_parity_computation <= '1' WHEN state = ENABLE_PARITY ELSE
                            '0';

  ena_parity_shift_register <= '1' WHEN state = STORE_PARITY_BIT ELSE
                               '0';

  rst_n_sync_parity_module <= '0' WHEN state = RESET_PARITY_MODULE ELSE
                              '1'; 
							  
  AW_ready <= '1' WHEN state = RECEIVE_AW ELSE
              '0';																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																 

  select_PUF_bit_index <= STD_LOGIC_VECTOR(select_PUF_bit_index_reg);
  
END ARCHITECTURE behavioural;
