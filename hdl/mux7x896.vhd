---------------------------------------------------------------------
--
-- Design unit:   Multiplexor
--
-- File name:     mux.vhd
--
-- Description:   Connect the oscillating RO cells to the counters
--
-- Autor:         Brice Colombier, Hubert Curien Laboratory, France
--
-- Copyright:     Hubert Curien Laboratory
--
-- Revision:      Version 1.00, June 2016
--
---------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY mux7x896 IS

  PORT (
    data_in   : IN  STD_LOGIC_VECTOR(895 DOWNTO 0);
    selection : IN  STD_LOGIC_VECTOR(6 DOWNTO 0);
    data_out  : OUT STD_LOGIC_VECTOR(6 DOWNTO 0));

END ENTITY mux7x896;

ARCHITECTURE rtl OF mux7x896 IS

BEGIN  -- ARCHITECTURE rtl

  WITH selection SELECT
    data_out <=
    data_in(895 DOWNTO 889) WHEN "1111111",
    data_in(888 DOWNTO 882) WHEN "1111110",
    data_in(881 DOWNTO 875) WHEN "1111101",
    data_in(874 DOWNTO 868) WHEN "1111100",
    data_in(867 DOWNTO 861) WHEN "1111011",
    data_in(860 DOWNTO 854) WHEN "1111010",
    data_in(853 DOWNTO 847) WHEN "1111001",
    data_in(846 DOWNTO 840) WHEN "1111000",
    data_in(839 DOWNTO 833) WHEN "1110111",
    data_in(832 DOWNTO 826) WHEN "1110110",
    data_in(825 DOWNTO 819) WHEN "1110101",
    data_in(818 DOWNTO 812) WHEN "1110100",
    data_in(811 DOWNTO 805) WHEN "1110011",
    data_in(804 DOWNTO 798) WHEN "1110010",
    data_in(797 DOWNTO 791) WHEN "1110001",
    data_in(790 DOWNTO 784) WHEN "1110000",
    data_in(783 DOWNTO 777) WHEN "1101111",
    data_in(776 DOWNTO 770) WHEN "1101110",
    data_in(769 DOWNTO 763) WHEN "1101101",
    data_in(762 DOWNTO 756) WHEN "1101100",
    data_in(755 DOWNTO 749) WHEN "1101011",
    data_in(748 DOWNTO 742) WHEN "1101010",
    data_in(741 DOWNTO 735) WHEN "1101001",
    data_in(734 DOWNTO 728) WHEN "1101000",
    data_in(727 DOWNTO 721) WHEN "1100111",
    data_in(720 DOWNTO 714) WHEN "1100110",
    data_in(713 DOWNTO 707) WHEN "1100101",
    data_in(706 DOWNTO 700) WHEN "1100100",
    data_in(699 DOWNTO 693) WHEN "1100011",
    data_in(692 DOWNTO 686) WHEN "1100010",
    data_in(685 DOWNTO 679) WHEN "1100001",
    data_in(678 DOWNTO 672) WHEN "1100000",
    data_in(671 DOWNTO 665) WHEN "1011111",
    data_in(664 DOWNTO 658) WHEN "1011110",
    data_in(657 DOWNTO 651) WHEN "1011101",
    data_in(650 DOWNTO 644) WHEN "1011100",
    data_in(643 DOWNTO 637) WHEN "1011011",
    data_in(636 DOWNTO 630) WHEN "1011010",
    data_in(629 DOWNTO 623) WHEN "1011001",
    data_in(622 DOWNTO 616) WHEN "1011000",
    data_in(615 DOWNTO 609) WHEN "1010111",
    data_in(608 DOWNTO 602) WHEN "1010110",
    data_in(601 DOWNTO 595) WHEN "1010101",
    data_in(594 DOWNTO 588) WHEN "1010100",
    data_in(587 DOWNTO 581) WHEN "1010011",
    data_in(580 DOWNTO 574) WHEN "1010010",
    data_in(573 DOWNTO 567) WHEN "1010001",
    data_in(566 DOWNTO 560) WHEN "1010000",
    data_in(559 DOWNTO 553) WHEN "1001111",
    data_in(552 DOWNTO 546) WHEN "1001110",
    data_in(545 DOWNTO 539) WHEN "1001101",
    data_in(538 DOWNTO 532) WHEN "1001100",
    data_in(531 DOWNTO 525) WHEN "1001011",
    data_in(524 DOWNTO 518) WHEN "1001010",
    data_in(517 DOWNTO 511) WHEN "1001001",
    data_in(510 DOWNTO 504) WHEN "1001000",
    data_in(503 DOWNTO 497) WHEN "1000111",
    data_in(496 DOWNTO 490) WHEN "1000110",
    data_in(489 DOWNTO 483) WHEN "1000101",
    data_in(482 DOWNTO 476) WHEN "1000100",
    data_in(475 DOWNTO 469) WHEN "1000011",
    data_in(468 DOWNTO 462) WHEN "1000010",
    data_in(461 DOWNTO 455) WHEN "1000001",
    data_in(454 DOWNTO 448) WHEN "1000000",
    data_in(447 DOWNTO 441) WHEN "0111111",
    data_in(440 DOWNTO 434) WHEN "0111110",
    data_in(433 DOWNTO 427) WHEN "0111101",
    data_in(426 DOWNTO 420) WHEN "0111100",
    data_in(419 DOWNTO 413) WHEN "0111011",
    data_in(412 DOWNTO 406) WHEN "0111010",
    data_in(405 DOWNTO 399) WHEN "0111001",
    data_in(398 DOWNTO 392) WHEN "0111000",
    data_in(391 DOWNTO 385) WHEN "0110111",
    data_in(384 DOWNTO 378) WHEN "0110110",
    data_in(377 DOWNTO 371) WHEN "0110101",
    data_in(370 DOWNTO 364) WHEN "0110100",
    data_in(363 DOWNTO 357) WHEN "0110011",
    data_in(356 DOWNTO 350) WHEN "0110010",
    data_in(349 DOWNTO 343) WHEN "0110001",
    data_in(342 DOWNTO 336) WHEN "0110000",
    data_in(335 DOWNTO 329) WHEN "0101111",
    data_in(328 DOWNTO 322) WHEN "0101110",
    data_in(321 DOWNTO 315) WHEN "0101101",
    data_in(314 DOWNTO 308) WHEN "0101100",
    data_in(307 DOWNTO 301) WHEN "0101011",
    data_in(300 DOWNTO 294) WHEN "0101010",
    data_in(293 DOWNTO 287) WHEN "0101001",
    data_in(286 DOWNTO 280) WHEN "0101000",
    data_in(279 DOWNTO 273) WHEN "0100111",
    data_in(272 DOWNTO 266) WHEN "0100110",
    data_in(265 DOWNTO 259) WHEN "0100101",
    data_in(258 DOWNTO 252) WHEN "0100100",
    data_in(251 DOWNTO 245) WHEN "0100011",
    data_in(244 DOWNTO 238) WHEN "0100010",
    data_in(237 DOWNTO 231) WHEN "0100001",
    data_in(230 DOWNTO 224) WHEN "0100000",
    data_in(223 DOWNTO 217) WHEN "0011111",
    data_in(216 DOWNTO 210) WHEN "0011110",
    data_in(209 DOWNTO 203) WHEN "0011101",
    data_in(202 DOWNTO 196) WHEN "0011100",
    data_in(195 DOWNTO 189) WHEN "0011011",
    data_in(188 DOWNTO 182) WHEN "0011010",
    data_in(181 DOWNTO 175) WHEN "0011001",
    data_in(174 DOWNTO 168) WHEN "0011000",
    data_in(167 DOWNTO 161) WHEN "0010111",
    data_in(160 DOWNTO 154) WHEN "0010110",
    data_in(153 DOWNTO 147) WHEN "0010101",
    data_in(146 DOWNTO 140) WHEN "0010100",
    data_in(139 DOWNTO 133) WHEN "0010011",
    data_in(132 DOWNTO 126) WHEN "0010010",
    data_in(125 DOWNTO 119) WHEN "0010001",
    data_in(118 DOWNTO 112) WHEN "0010000",
    data_in(111 DOWNTO 105) WHEN "0001111",
    data_in(104 DOWNTO 98)  WHEN "0001110",
    data_in(97 DOWNTO 91)   WHEN "0001101",
    data_in(90 DOWNTO 84)   WHEN "0001100",
    data_in(83 DOWNTO 77)   WHEN "0001011",
    data_in(76 DOWNTO 70)   WHEN "0001010",
    data_in(69 DOWNTO 63)   WHEN "0001001",
    data_in(62 DOWNTO 56)   WHEN "0001000",
    data_in(55 DOWNTO 49)   WHEN "0000111",
    data_in(48 DOWNTO 42)   WHEN "0000110",
    data_in(41 DOWNTO 35)   WHEN "0000101",
    data_in(34 DOWNTO 28)   WHEN "0000100",
    data_in(27 DOWNTO 21)   WHEN "0000011",
    data_in(20 DOWNTO 14)   WHEN "0000010",
    data_in(13 DOWNTO 7)    WHEN "0000001",
    data_in(6 DOWNTO 0)     WHEN OTHERS;


END ARCHITECTURE rtl;