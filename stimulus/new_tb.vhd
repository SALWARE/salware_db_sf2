----------------------------------------------------------------------
-- Created by Microsemi SmartDesign Mon Feb 27 14:25:18 2017
-- Testbench Template
-- This is a basic testbench that instantiates your design with basic 
-- clock and reset pins connected.  If your design has special
-- clock/reset or testbench driver requirements then you should 
-- copy this file and modify it. 
----------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: new_tb.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::SmartFusion2> <Die::M2S025> <Package::484 FBGA>
-- Author: <Name>
--
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

entity new_tb is
end new_tb;

architecture behavioral of new_tb is

    constant SYSCLK_PERIOD : time := 100 ns; -- 10MHZ

    signal SYSCLK : std_logic := '0';
    signal NSYSRESET : std_logic := '0';

    component db_top_inst
        -- ports
        port( 
            -- Inputs
            XTL : in std_logic;
            DATA3_N : in std_logic;
            GPIO_0 : in std_logic;
            GPIO_1 : in std_logic;
            GPIO_2 : in std_logic;

            -- Outputs
            DATA0_P : out std_logic;
            DATA0_N : out std_logic;
            DATA1_P : out std_logic;
            DATA1_N : out std_logic;
            DATA2_P : out std_logic;
            DATA2_N : out std_logic;
            DATA3_P : out std_logic;
            LED_OUT : out std_logic

            -- Inouts

        );
    end component;

begin

    process
        variable vhdl_initial : BOOLEAN := TRUE;

    begin
        if ( vhdl_initial ) then
            -- Assert Reset
            NSYSRESET <= '0';
            wait for ( SYSCLK_PERIOD * 10 );
            
            NSYSRESET <= '1';
            wait;
        end if;
    end process;

    -- Clock Driver
    SYSCLK <= not SYSCLK after (SYSCLK_PERIOD / 2.0 );

    -- Instantiate Unit Under Test:  db_top_inst
    db_top_inst_0 : db_top_inst
        -- port map
        port map( 
            -- Inputs
            XTL => SYSCLK,
            DATA3_N => '0',
            GPIO_0 => '0',
            GPIO_1 => '0',
            GPIO_2 => '0',

            -- Outputs
            DATA0_P =>  open,
            DATA0_N =>  open,
            DATA1_P =>  open,
            DATA1_N =>  open,
            DATA2_P =>  open,
            DATA2_N =>  open,
            DATA3_P =>  open,
            LED_OUT =>  open

            -- Inouts

        );

end behavioral;

